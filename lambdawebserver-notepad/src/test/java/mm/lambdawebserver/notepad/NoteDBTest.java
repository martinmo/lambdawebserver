package mm.lambdawebserver.notepad;

import mm.lambdawebserver.extensions.db.DB;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class NoteDBTest {

    private NoteDB noteDB;

    @Before
    public void before(){
        final DataSource dataSource = DB.initHSQLMemDB();
        noteDB = new NoteDB(dataSource);

        noteDB.initDB(true);
    }

    @Test
    public void testFind() throws Exception {
        final Note inserted = noteDB.insert(Note.note("1", "content"));

        assertThat(inserted, CoreMatchers.notNullValue());
        assertThat(inserted.getId(), equalTo("1"));
        assertThat(inserted.getUpdated(), CoreMatchers.notNullValue());

        assertThat(noteDB.find("1").isPresent(), is(true));
    }

    @Test
    public void testUpdate() throws Exception {
        final Note inserted = noteDB.insert(Note.note("1", "content"));

        inserted.setContent("2");

        noteDB.update(inserted);

        assertThat(noteDB.find("1").get().getContent(), equalTo("2"));
    }

    @Test
    public void testDelete() throws Exception {
        final Note inserted = noteDB.insert(Note.note("1", "content"));

        noteDB.delete(inserted.getId());

        assertThat(noteDB.list().size(), is(0));
    }
}