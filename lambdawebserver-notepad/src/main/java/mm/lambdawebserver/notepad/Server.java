package mm.lambdawebserver.notepad;

import com.google.common.collect.ImmutableMap;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.api.Wrapper;
import mm.lambdawebserver.extensions.db.DB;
import mm.lambdawebserver.impl.UndertowHttpServer;

import javax.sql.DataSource;
import java.util.Optional;
import java.util.UUID;

import static mm.lambdawebserver.api.Extractor.*;
import static mm.lambdawebserver.api.Response.temporaryRedirect;

public class Server {

    public static void main(final String args[]) throws Exception {

        final DataSource dataSource = DB.initHSQLFileDB("./db/notes.db/");
        final NoteDB noteDB = new NoteDB(dataSource);
        final Templates templates = new Templates();

        noteDB.initDB(Boolean.getBoolean("dropdb"));

        final LambdaHTTPServer server = UndertowHttpServer.bind(80);

        server.around("/*", Wrapper.GZIP);
        server.around("/*", Wrapper.TIMING);

        server.onError(e -> r -> templates.error(e.getMessage(), 500));
        server.around("/*", handler -> r -> {
            final Response response = handler.apply(r);
            if(response.code == 404){
                return templates.error("not found", 404);
            }else {
                return response;
            }
        });

        server.post("/*", extract2(STRING, BODY_URL_ENCODED, id -> map -> r -> {
                    final Note note = Note.note(id, map.get("content"));
                    note.setUpdated(Long.parseLong(map.get("updated")));
                    final Note updated = noteDB.update(note);
                    return Response.movedPermanently("/" + updated.getId());
                }
        ));

        server.post("/delete/*", extract(STRING, id -> r -> {
            noteDB.delete(id);
            return Response.movedPermanently("/list");
        }));

        server.get("/*", extract(STRING, id -> r -> templates.render("note", noteDB.find(id))));

        server.get("/", r -> {
            final Note note = noteDB.insert(Note.note(UUID.randomUUID().toString(), ""));
            return temporaryRedirect("/" + note.getId());
        });

        server.get("/list", r -> templates.render("index",
                Optional.of(ImmutableMap.builder().put("notes",noteDB.list()).build())
        ));
    }
}
