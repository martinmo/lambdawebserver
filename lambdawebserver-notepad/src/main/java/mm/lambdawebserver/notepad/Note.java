package mm.lambdawebserver.notepad;

import com.google.common.escape.Escaper;
import com.google.common.html.HtmlEscapers;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static com.google.common.base.Preconditions.checkNotNull;

public class Note {

    private String id;
    private String content;
    private Instant updated;

    public Note() {
    }

    public static Note note(final String id, final String content){
        checkNotNull(id);
        checkNotNull(content);
        final Note note = new Note();
        note.id = id;
        note.content = content;
        note.updated = Instant.now();
        return note;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        checkNotNull(id);
        this.id = id;
    }

    public String getContent() {
        checkNotNull(content);
        return content;
    }

    public void setContent(String content) {
        this.content = HtmlEscapers.htmlEscaper().escape(content);
    }

    public long getUpdated() {
        return updated.getEpochSecond();
    }

    public void setUpdated(long updated) {
        this.updated = Instant.ofEpochSecond(updated);
    }

    public String getUpdatedAsString(){
        LocalDateTime localDate = LocalDateTime.ofInstant(updated, ZoneId.systemDefault());
        return localDate.toString();
    }

    public String firstLine() {
        return content.split(System.getProperty("line.separator"))[0];
    }
}
