package mm.lambdawebserver.notepad;

import mm.lambdawebserver.extensions.db.DB;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

public class NoteDB {

    private final DB.DbInstance db;

    public NoteDB(DataSource dataSource) {
        db = DB.prepare(dataSource);
    }

    public void initDB(final boolean drop) {
        db.tx(con -> {

            if (drop) {
                con.update("drop table IF EXISTS notes");
            }

            con.update("CREATE TABLE IF NOT EXISTS notes ( id VARCHAR(128) NOT NULL, content VARCHAR(6000) NOT NULL, updated BIGINT NOT NULL);");
            return Void.TYPE;
        });
    }

    public List<Note> list() {
        return db.tx(con ->
                        con.queryMultiple(Note.class,
                                "select * from notes LIMIT 2000"
                        )
        );
    }

    public Optional<Note> find(final String id) {
        Note note = db.tx(con -> getNoteById(id, con));
        return Optional.ofNullable(note);
    }


    public Note insert(Note note) {
        return db.tx(con -> {
            final int update = con.update("insert into notes values (?, ?, ?)", note.getId(), note.getContent(), note.getUpdated());
            if (update != 1) {
                throw new RuntimeException("insert failed " + note);
            } else {
                return getNoteById(note.getId(), con);
            }
        });
    }

    private Note getNoteById(String id, DB.ConnectionWrapper con) {
        return con.querySingle(Note.class, "select * from notes where id = ?", id);
    }

    public Note update(Note note) {
        return db.tx(con -> {
            final int update = con.update("update notes set content = ? where id = ? and updated = ?",
                    note.getContent(), note.getId(), note.getUpdated());
            checkState(update == 1, "number of updated lines must be 1 but was %s ", update);
            assert update == 1;
            return getNoteById(note.getId(), con);
        });
    }

    public void delete(final String id) {
        db.tx(con -> {
            final int update = con.update("delete from notes where id = ?", id);
            checkArgument(update == 1, "not found");
            return Void.TYPE;
        });
    }
}
