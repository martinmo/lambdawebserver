package mm.lambdawebserver.notepad;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.common.collect.ImmutableMap;
import mm.lambdawebserver.api.Response;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Templates {

    private MustacheFactory mf = new DefaultMustacheFactory();

    private Map<String, Mustache> templates = new HashMap<>();

    // not threadsafe!
    public Response render(final String template, Optional data) {
        if (!data.isPresent()) {
            return Response.notFound();
        }
        return buildResponse(200, getTemplate(template), data.get());
    }

    private Mustache getTemplate(String template) {
        return templates.computeIfAbsent(template, name ->
                        mf.compile(template + ".mustache")
        );
    }

    private Response buildResponse(final int code, final Mustache mustache, final Object data) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            mustache.execute(new PrintWriter(out), data).flush();
            return Response.bytes(out.toByteArray()).asHTML().changeCode(code);
        } catch (IOException e) {
            e.printStackTrace();
            return Response.error(e.getMessage());
        }
    }

    public Response error(String message, final int code) {
        final Map<String, Object> data = ImmutableMap.<String, Object>builder().put("message", message).put("code", code).build();
        return buildResponse(code, getTemplate("error"), data);
    }

}
