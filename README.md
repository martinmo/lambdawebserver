# λ HTTP server (DRAFT) #

This is a small experimental library to build HTTP APIs with Java 8's lambdas. It provides a lightweight api to register handlers which transform a request to a response. 
It does not implement the HTTP protocol handling itself, but uses Servlet 3.1, [Undertow](http://undertow.io/) or [Sun's HTTPServer](http://docs.oracle.com/javase/8/docs/jre/api/net/httpserver/spec/com/sun/net/httpserver/HttpServer.html) for the heavy lifting. It is inspired by [Spark](http://www.sparkjava.com/) and [fluent-http](https://github.com/CodeStory/fluent-http) but has a more functional interface. Current state is experimental.

* Version: 0.1-SNAPSHOT

## Getting started ##

git clone

mvn clean install

```
#!xml
<dependency>
    <groupId>mm.lambdawebserver</groupId>
    <artifactId>lambdawebserver-core</artifactId>
    <version>0.1-SNAPSHOT</version>
</dependency>
```

In Java

```
#!java
LambdaHTTPServer server =  UndertowHttpServer.bind(80);
server.get("/ping", r -> Response.ok("pong"));
server.post("/echo", r -> Response.ok(r.bodyAsString()));
// later on
server.stop();
```

## Manual ##

See tests to dive in directly 
> /lambdawebserver-core/src/test/java/mm/lambdawebserver/it/ShowCaseTest.java

λ HTTP server is build around four main classes.

* `Request` an HTTP Request
* `Repsonse` an HTTP Response
* `RequestHandler` a function from Request to Response
* `LambdaHTTPServer` register your handler here

### RequestHandler ###
> Request -> Response

RequestHandler is a `@FunctionalInterface` and can be implemented like `request -> Response.ok("ok")`. RequestHandler is the central concept and nearly everything is build around it.

```
#!java
public interface LambdaHTTPServer {
    void get(String path, RequestHandler f);

    void post(String path, RequestHandler f);

    void delete(String path, RequestHandler f);

    void put(String path, RequestHandler f);
    
    void handle(Method method, String path, RequestHandler handler);

    void before(String path, BeforeFilter filter);

    void around(String path, Wrapper wrapper);

    void onError(Function<Throwable, RequestHandler> catcher);

    void stop();
}
```

### Request and Response ###

```
#!java
public class Request {
    public final URI uri;
    public final InputStream body;
    public final Map<String, String> params;
    public final Method method;
    public final Headers headers;
    public final Optional<String> fragment;
    ...
}
```
    
Use Response like `Response.ok("your content").as("text/plain")`

```
#!java
public class Response {
   
    public final int code;
    public final Headers headers;
    public final BodyContent body;

    public static Response ok(final String message) {...}
    public static Response ok(final Object object){...}
    public static Response file(final String dir, final String path) {...}   
    public static Response code(final int code){...}
    public static Response error(final String cause) {...}
    public Response as(final String contentType) {...}
    public Response withHeader(final String name, final String value){...}
    public Response cacheFor(Duration duration){...}
    ...
}
```

### Routing and fragments ###
When looking up the handler, the routing engine uses the first exact match then longest path match.
You can use wildcard syntax to match a bunch of paths like `server.get("/resource/*", RequestHandler)`. None empty fragments matched by `*` will be accessible in the Request object as `Optional<String> fragment`.

```
#!java
server.get("/resource/*", r -> Response.ok(r.fragment.orElse("path fragment is empty")));
```

```
#!java
server.get("/p*", r -> Response.ok("4"));
server.get("/p", r -> Response.ok("3"));
server.get("/*", r -> Response.ok("2"));
server.get("/", r -> Response.ok("1"));

get("/").then().assertThat().body(equalTo("1"));
get("/a").then().assertThat().body(equalTo("2"));
get("/p").then().assertThat().body(equalTo("3"));
get("/pp").then().assertThat().body(equalTo("4"));
```

### Working with JSON ###
Serialisation is done with [Jackson](https://github.com/FasterXML/jackson). Returning JSON is as easy as

```
#!java
server.get("/data", r -> Response.ok(new Data()));
```

Parsing can be done with Request's helper method  `Optional<T> getData(Class<T> clazz)` like this

```
#!java
server.post("/data", r ->
    r.getData(Data.class)
        .map(data -> Response.ok(data)).orElse(Response.error("failed to parse")
));
```
 
or using an extractor like

```
#!java
server.post("/*", extract(JSON(Data.class), data -> r -> Response.ok(data)));
```


### Staying DRY with Extractors ###
Parse a fragment to Integer without using extractors

```
#!java
private static Optional<Integer> toInt(final String str){...}

server.get("/*", r -> r.fragment.flatMap(fragment -> toInt(fragment))
    .map(i -> Response.ok("got Integer " + i))
    .orElse(Response.error("failed to parse Integer")));
```

Using extractors

```
#!java
server.get("/*", extract(INT, i -> r -> Response.ok("got Integer " + i)));
```

An Extractors signature looks like

> Request -> Optional<I>

The extract function has following signature. It returns a `HTTP 400 Bad Request` if the extractor evaluates to `Optional.empty()`.

>  (Extractor<T>, T -> RequestHandler) -> RequestHandler

Uncurried version:

>  (Extractor<T>, T -> Request -> Response) -> RequestHandler

Implement it using lambdas

```
#!java
extract(INT, (Integer i) -> request -> Response.ok("got Integer " + i));
```


Writing your own extractor is easy

```
#!java
public static Extractor<Long> LONG =
    (request) -> request.fragment.flatMap(f -> {
        try {
            return Optional.of(Long.parseLong(request.fragment.get()));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    });
```

You can use two extractors to extract two values from request as well (including the body or query parameters)

```
#!java
server.post("/*", extract2(INT, BODY_INT, i -> j -> 
    r -> Response.ok(i + j + "")
));
```

As you can see extractors do not appear in `LambdaHTTPServer` but are build with function composition around `RequestHandler`. You are not limited to parsing Integers and the like, but you could even fetch data from a DB for instance. Using an extractor that takes a cookie and fetches a user from a DB could look like this:

```
#!java
server.get("/somthing/*", extract(USER, (User user) -> r -> Response.ok("Hi " + user.name)));
```

### More curry ###
Building on top of `RequestHandler` is not limited to parsing `Request`s but can be used to provide other kinds of context as well. Look at the Database example in `lambdawebserver-examples` for DBRequestHandler. This handler provides database connection running a transaction for each request:

```
#!java
server.get("/links", withDb.apply(
    connection -> r -> Response.ok(LinkDao.list(connection))
));
```

If you cannot resist and want some kind of reflection based injection you could use a pattern like this:

```
#!java
Supplier<RequestHandler> instanceSupplier = () -> {
    // somehow prepare an instance of the request handler
    System.out.println("preparing instance");
    return r -> Response.ok("ok");
};

server.get("/", r -> instanceSupplier.get().apply(r));
```

If you have a lot of CRUD operations you could introduce some kind of resource interface like

```
#!java
// I == Class of Id/Key
// E == Class of Entity
interface Resource<ID, E> {

    Class<E> getResourceClass();

    // returns id
    Optional<ID> create(E newObject);

    Optional<E> update(ID id, E toUpdate);

    Optional<E> get(ID id);

    List<E> list();

    // id if removed
    Optional<ID> delete(ID id);
}
```

which can be served like `server.handle(Method.ANY, "/users/*", ResourceHandler.handle(Extractor.STRING, userResource));`. See the example in `lambdawebserver-examples` for details.

### BeforeFilters ###
> Request -> Optional<Response>

A before filter gets applied before your RequestHandler gets invoked. It can finish a Request by returning Optional.of(Result) or not.
Have a look at `BeforeFilter.auth(final String realm, final BiPredicate<String, String> credentialsChecker)` to see how it can be used to implement Http Basic Authentication in a slick way.  

Register it with `void before(String path, BeforeFilter filter)`

```
#!java
server.before("/*", r -> {
            System.out.println(r);
            return Optional.empty();
        });
```

### Wrappers ###
> RequestHandler -> RequestHandler

A wrapper is more general concept than the BeforeFilter. Actually BeforeFilters are implemented as a Wrapper. See `mm.lambdawebserver.api.Wrapper.TIMING` for a Wrapper that prints the time that your RequestHandler needs to generate the Response. More than one Wrapper (or BeforeFilter) can be registered for a route. All matching Wrapper will get applied if a Request comes in.

### Using  λ HTTP server in unit tests ###
Mocking an HTTP API is really easy using this API. But actually I ran into an issue when starting a server for each test case. 
The problem was that the OS needed some time to make the TCP port available for binding after stopping the server. The symptom is a `java.net.BindException: Address already in use` Exception. A workaround is to use a different port for each case. See `AbstractServerTest` for an example of this can be achieved. With port rotation IntelliJ reports 3 seconds for 92 test cases, starting and stopping a server for each.  


### REST and have a book ###

```
#!java
public class BookServer {

    // threadsafe singleton service
    BookService bookService = new BookService();
    private LambdaHTTPServer s;

    public BookServer(final int port) throws IOException {
        bookService.insert(BookService.Book.of("The Lord of the Rings", "John Ronald Reuel Tolkien",
                "When Mr. Bilbo Baggins of Bag End announced that he would shortly be celebrating " +
                "his eleventy-first birthday with a party of special magnificence, " +
                "there was much talk and excitement in Hobbiton. ..."
        ));

        s = UndertowHttpServer.bind(port);

        s.around("/*", Wrapper.TIMING);

        s.get("/books/", r -> Response.ok(bookService.all()));

        s.get("/books/*", extract(STRING, id -> r ->
                        bookService.find(id).map(Response::ok).orElse(Response.notFound())
        ));

        s.delete("/books/*", extract(STRING, id -> r -> {
            final boolean deleted = bookService.delete(id);
            if (deleted) {
                return Response.ok("deleted");
            } else {
                return Response.notFound("book with id " + id);
            }
        }));

        s.put("/books/*", extract2(STRING, JSON(BookService.Book.class),
                id -> book -> r -> {
                    bookService.update(id, book);
                    return Response.ok("updated");
                }
        ));

        s.post("/books/", extract(JSON(BookService.Book.class), book -> r -> {
            final BookService.Book newBook = bookService.insert(book);
            return Response.redirect("/books/" + newBook.id);
        }));

        // this serves an AngularJS app
        s.get("/", r -> Response.file("mm/lambdawebserver/examples/restlike/bookstore/index.html"));
    }

    public void stop() {
        s.stop();
    }

    public static void main(final String[] args) throws IOException {
        final BookServer bookServer = new BookServer(80);
        System.in.read();
        bookServer.stop();
    }
}

```

### Limitations ###

* in memory request body / max request size
* TODO 

## Build with open source software ##

Thanks to

* junit
* jackson
* slf4j
* guava
* rest-assured
* commons-io
* undertow
* and more ...

See pom.xml for all dependencies.

## Personal Notes ##
TODO

## Todo ##
* Refactor routing engine
* make Headers.java immutable
* Add helper methods for Cookie handling
* Add bean validation helpers? (i.e. extractValid)
* Review exception handling
* Support Futures (Future<Response>)
* Streaming Support (i.e. SSE) / lazy bodies
* Resource example
* Cleanup tests

## Contribution guidelines ##

* You are very welcome 
* Write tests
* Be kind

# Licence #
The MIT License (MIT)

Copyright (c) 2014 Martin Möller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.