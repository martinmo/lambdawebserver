package mm.lambdawebserver.it;

import com.jayway.restassured.http.ContentType;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.function.Function;

import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

public class FilesTest extends AbstractServerTest {

    public FilesTest(Function<Integer, LambdaHTTPServer> serverBuilder) {
        super(serverBuilder);
    }

    @Test
    public void canServeFile() {
        server.get("/", r -> Response.file("static/data.json"));
        get("/").then().assertThat().
                statusCode(200)
                .and().body("data", equalTo("from file"))
                .and().contentType(ContentType.JSON);
    }

    @Test
    public void canServeFiles() {
        server.get("/*", r -> Response.serverFile("/static", r).asJSON());
        get("/dir/data.txt").then().assertThat().body("data", equalTo("from file in dir"));
    }

    @Test
    public void notFoundFile() {
        server.get("/", r -> Response.file("data.text").asJSON());
        get("/").then().assertThat().statusCode(404);
    }

    @Test
    @Ignore // invalid url in jetty
    public void canNotEscapeDir() {
        server.get("/*", r -> Response.serverFile("/static", r));
        get("/../secret.txt").then().assertThat().statusCode(404);
    }

    @Test
    public void canServeExistentExternalFile() {
        final String currentPath = System.getProperty("user.dir");

        File file = new File(currentPath, "test.file");
        file.deleteOnExit();
        try {
            file.createNewFile();
            server.get("/", r -> Response.externalFile(currentPath, "test.file"));

            get("/").then().assertThat().statusCode(200);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            file.delete();
        }
    }

    @Test
    public void noneExistentExternalFileCauses404() {
        server.get("/", r -> Response.externalFile("lambdawebserver-core/src/test/resources/static/", "no"));

        get("/").then().assertThat().statusCode(404);
    }

    @Test
    public void externalFileOnDirectoryCauses404() {
        server.get("/", r -> Response.externalFile("lambdawebserver-core/src/test/resources/static/", ""));

        get("/").then().assertThat().statusCode(404);
    }
}