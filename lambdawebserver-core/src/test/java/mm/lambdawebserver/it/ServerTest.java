package mm.lambdawebserver.it;

import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Function;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class ServerTest extends AbstractServerTest {
    public ServerTest(Function<Integer, LambdaHTTPServer> serverBuilder) {
        super(serverBuilder);
    }

    @Test
    @Ignore
    public void canRestrictBodySize() {
        server.post("/", r -> {
            final boolean consume = consume(r.body, 1024 * 2);
            if (consume) {
                return Response.ok("" + 1024 * 2);
            } else {
                return Response.error("");
            }
        });

        byte[] body = new byte[1024 * 2];

        given().body(body).when().post().then().assertThat().statusCode(200).and().body(equalTo(1024 * 2 + ""));
    }

    @Test
    @Ignore
    public void cannotServeMoreThanMaxBodySize() {
        server.post("/", r -> {
            final boolean consume = consume(r.body, 1024 * 2);
            if (consume) {
                return Response.ok("" + 1024 * 2);
            } else {
                return Response.error("");
            }
        });

        byte[] body = new byte[(1024 * 2) + 1];

        given().body(body).when().post().then().assertThat().statusCode(500);
    }

    private boolean consume(InputStream in, int num) {
        try {
            for (int i = 0; i < num; i++) {
                in.read();
            }
            if(in.available() > 0){
                return false;
            }

            return true;
        } catch (IOException e) {
            return false;
        }finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
