package mm.lambdawebserver.it;

import com.jayway.restassured.http.ContentType;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import org.junit.Test;

import java.util.function.Function;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class BasicTest extends AbstractServerTest {

    public BasicTest(Function<Integer, LambdaHTTPServer> serverBuilder){
        super(serverBuilder);
    }

    @Test
    public void starts() {
        server.get("/", r -> Response.ok("ok"));
        get("/").then().assertThat().body(equalTo("ok")).and().contentType(ContentType.TEXT);
    }

    @Test
    public void notFound() {
        server.get("/sdf", r -> Response.ok("ok"));
        get("/notfound").then().assertThat().statusCode(404).and().contentType(ContentType.TEXT);
    }

    @Test
    public void json() {

        final String json = "{\"data\": \"data\"}";

        server.get("/", r -> Response.ok(json).as("application/json"));
        get("/").then().assertThat().body("data", equalTo("data")).and().contentType(ContentType.JSON);
    }

    @Test
    public void fragments() {
        server.get("/*", r -> Response.ok(r.fragment.get()));
        get("/1").then().assertThat().body(equalTo("1")).and().contentType(ContentType.TEXT);
        get("/1/2").then().assertThat().body(equalTo("1/2")).and().contentType(ContentType.TEXT);
        get("/1/2/3").then().assertThat().body(equalTo("1/2/3")).and().contentType(ContentType.TEXT);
    }

    @Test
    public void fragmentsPartial() {
        server.get("/test/*", r -> Response.ok(r.fragment.get()));
        get("/test/1").then().assertThat().body(equalTo("1")).and().contentType(ContentType.TEXT);
    }


    @Test
    public void defaultErrorHandling(){
        server.get("/handler", r -> {throw new RuntimeException("fail");});
        get("/handler").then().assertThat().statusCode(500).and().body(equalTo("fail"));

        server.before("/filter", r -> {throw new RuntimeException("filter");});
        get("/filter").then().assertThat().statusCode(500).and().body(equalTo("filter"));
    }

    @Test
    public void customErrorHandling(){
        server.get("/handler", r -> {throw new RuntimeException("fail");});
        server.onError( e -> request -> Response.ok("no error"));
        get("/handler").then().assertThat().statusCode(200).and().body(equalTo("no error"));

        server.before("/filter", r -> {throw new RuntimeException("filter");});
        get("/filter").then().assertThat().statusCode(200).and().body(equalTo("no error"));
    }

    @Test
    public void getJSONBeanIsEmptyOnFail(){
        server.post("/", r -> Response.ok(r.getData(Data.class).map(d -> d.data).orElse("no")));
        given().body("some body").when().post("/").then().assertThat().statusCode(200).and().body(equalTo("no"));
    }

}
