package mm.lambdawebserver.it;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.RequestHandler;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.impl.UndertowHttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class ShowCaseTest {
    final static AtomicInteger portProvider = new AtomicInteger(9000);
    LambdaHTTPServer server;

    @Before
    public void init() throws IOException {
        final int port = portProvider.getAndIncrement();
        server = UndertowHttpServer.bind(port);
        RestAssured.port = port;
    }

    @Test
    public void basicRequestHandling() {
        server.get("/ping", r -> Response.ok("pong"));

        when().get("/ping").then().assertThat()
                .contentType(ContentType.TEXT)
                .and().statusCode(200)
                .and().body(equalTo("pong"));

        server.post("/echo", r -> Response.ok(r.bodyAsString()));

        given().body("hello echo").when().post("/echo").then().assertThat()
                .body(equalTo("hello echo"));
    }

    @Test
    public void pathFragments(){
        server.get("/resource/*", r -> Response.ok(r.fragment.orElse("path fragment is empty")));

        when().get("/resource/1").then().assertThat().
                body(equalTo("1"));

        when().get("/resource/").then().assertThat().
                body(equalTo("path fragment is empty"));
    }

    @Test
    public void queryParams(){
        server.get("/list", r -> Response.ok(r.getParam("sortBy").orElse("default")));

        when().get("/list?sortBy=name").then().assertThat().body(equalTo("name"));

        when().get("/list").then().assertThat().body(equalTo("default"));
    }


    public static class Data {
        public String data = "the data";
    }

    @Test
    public void workingWithJSON() {

        server.get("/data", r -> Response.ok(new Data()));

        when().get("/data").then().assertThat()
                .contentType(ContentType.JSON)
                .and().statusCode(200)
                .and().body(equalTo("{\"data\":\"the data\"}"));

        // echo Data.class
        server.post("/data", r ->
                        r.getData(Data.class).map(data -> Response.ok(data)).orElse(Response.error("failed to parse"))
        );

        given().body(new Data()).when().post("/data").
                then().assertThat().body("data", equalTo("the data"));
    }

    @Test
    public void servingFiles() throws InterruptedException {
        server.get("/favicon.ico", r -> Response.file("static/img/favicon.ico"));

        when().get("/favicon.ico").then().assertThat()
                .statusCode(200).and().contentType("image/x-icon");

        server.get("/assets/*", RequestHandler.serveFiles("/static/dir"));

        when().get("/assets/data.txt").then().assertThat()
                .contentType(ContentType.TEXT)
                .and().body(equalTo("{ \"data\": \"from file in dir\"}"));
    }

    @Test
    public void beforeFilter(){
        server.before("/*", r -> {
            System.out.println(r);
            return Optional.empty();
        });
    }

    @After
    public void end() {
        server.stop();
    }
}
