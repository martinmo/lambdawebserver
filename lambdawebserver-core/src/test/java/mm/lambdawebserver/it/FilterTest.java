package mm.lambdawebserver.it;


import com.jayway.restassured.http.ContentType;
import mm.lambdawebserver.api.BeforeFilter;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import org.hamcrest.MatcherAssert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class FilterTest extends AbstractServerTest {

    public FilterTest(Function<Integer, LambdaHTTPServer> serverBuilder){
        super(serverBuilder);
    }

    @Test
    public void filterBeforeHandler() {
        final List<String> order = new LinkedList<>();
        server.get("/", r -> {
            order.add("handler");
            return Response.ok("ok");
        });
        server.before("/", r -> {
            order.add("filter");
            return Optional.empty();
        });
        get("/").then().assertThat().body(equalTo("ok")).and().contentType(ContentType.TEXT);

        MatcherAssert.assertThat(order.get(0), equalTo("filter"));
        MatcherAssert.assertThat(order.get(1), equalTo("handler"));
    }

    @Test
    public void filterCanFinishRequest() {
        server.get("/", r -> Response.ok("ok"));
        server.before("/", r -> Optional.of(Response.ok("filter")));

        get("/").then().assertThat().body(equalTo("filter"));
    }

    @Test
    public void multipleFilters() {
        AtomicInteger integer = new AtomicInteger();
        server.before("/", (r) -> {
            integer.getAndIncrement();
            return Optional.empty();
        });
        server.before("/", (r) -> {
            integer.getAndIncrement();
            return Optional.empty();
        });
        get("/").then().assertThat().statusCode(404);
        assertThat(integer.get(), equalTo(2));
    }

    @Test
    public void moreOneMultipleFilters() {
        AtomicInteger integer = new AtomicInteger();
        server.before("/some", (r) -> {
            integer.getAndIncrement();
            return Optional.empty();
        });
        server.before("/*", (r) -> {
            assert(!r.fragment.isPresent());
            integer.getAndIncrement();
            return Optional.empty();
        });
        get("/some").then().assertThat().statusCode(404);
        assertThat(integer.get(), equalTo(2));
    }


    @Test
    public void auth() {
        server.before("/", BeforeFilter.auth("test", (u, p) -> u.equals(p)));
        get("/").then().assertThat().statusCode(401).and().header("WWW-Authenticate", containsString("test"));

        given().auth().basic("username", "username").when().get("/").then().statusCode(404);
    }

    @Test
    public void invalidAuthReturns500() {
        server.before("/", BeforeFilter.auth("test", (u, p) -> u.equals(p)));
        given().header("Authorization", "dsafsdafsdafsadf").when().get("/").then().statusCode(500);
    }


}
