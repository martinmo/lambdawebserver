package mm.lambdawebserver.it;

import com.jayway.restassured.RestAssured;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.impl.servlet.AbstractServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

public class ServletContextTest {

    public static class ContextServlet extends AbstractServlet {

        @Override
        public void configure(LambdaHTTPServer server) {
            server.get("/*", r -> Response.ok(r.fragment.get()));
        }
    }

    @Test
    public void servletTest() throws Exception {
        RestAssured.port = 7080;

        final ServletContextHandler context = new ServletContextHandler(null, "/test/*");
        context.addServlet(ContextServlet.class, "/");

        final Server server = new Server(7080);
        server.setHandler(context);
        server.start();

        get("/test/1frag").then().statusCode(200).and().body(equalTo("1frag"));

        server.stop();
    }

}
