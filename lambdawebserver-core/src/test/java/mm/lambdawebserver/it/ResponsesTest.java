package mm.lambdawebserver.it;

import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import org.junit.Test;

import java.util.function.Function;

import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

public class ResponsesTest extends AbstractServerTest {
    public ResponsesTest(Function<Integer, LambdaHTTPServer> serverBuilder) {
        super(serverBuilder);
    }

    @Test
    public void canRedirect() {
        server.get("/", r -> Response.movedPermanently("/re"));
        server.get("/re", r -> Response.ok("ok"));

        get("/").then().assertThat().body(equalTo("ok"));
    }


}
