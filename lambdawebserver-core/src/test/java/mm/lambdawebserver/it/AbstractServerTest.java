package mm.lambdawebserver.it;


import com.jayway.restassured.RestAssured;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.impl.SunHTTPServer;
import mm.lambdawebserver.impl.UndertowHttpServer;
import mm.lambdawebserver.impl.servlet.ServletRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

@RunWith(value = Parameterized.class)
public abstract class AbstractServerTest {
    protected LambdaHTTPServer server;
    private final static AtomicInteger port = new AtomicInteger(4000);

    Function<Integer, LambdaHTTPServer> serverBuilder;

    public AbstractServerTest(Function<Integer, LambdaHTTPServer> serverBuilder){
        this.serverBuilder = serverBuilder;
    }

    @Parameterized.Parameters
    public static Collection data() {
        Function<Integer, LambdaHTTPServer> sun = (port) -> {
            try {
                return SunHTTPServer.bind(port);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        };
        Function<Integer, LambdaHTTPServer> under = (port) -> {
            try {
                return UndertowHttpServer.bind(port);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        };
        Function<Integer, LambdaHTTPServer> servlet = (port) -> {
            try {
                return ServletRunner.bind(port);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        };
        return Arrays.asList(new Object[]{sun}, new Object[]{under}, new Object[]{servlet});
    }

    @Before
    public void before() throws IOException {
        int currentPort = port.getAndIncrement();
        RestAssured.port = currentPort;
        server = serverBuilder.apply(currentPort);
    }

    @After
    public void after() {
        server.stop();
    }

}
