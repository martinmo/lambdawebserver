package mm.lambdawebserver.it;

import mm.lambdawebserver.api.Headers;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Request;
import mm.lambdawebserver.api.Response;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class HeaderTest extends AbstractServerTest {

    public HeaderTest(Function<Integer, LambdaHTTPServer> serverBuilder) {
        super(serverBuilder);
    }

    @Test
    public void canReadHeaders() {
        final List<Request> requests = new ArrayList<>(1);

        server.get("/", r -> {
            requests.add(r);
            return Response.ok("ok");
        });
        given().header("X-Test", "test").when().get("/")
                .then().assertThat().statusCode(200).and().body(equalTo("ok"));

        final Headers headers = requests.get(0).headers;
        assertThat(headers.getFirst("X-Test").get(), equalTo("test"));
    }

    @Test
    public void canReadMultiValueHeaders() {
        final List<Request> requests = new ArrayList<>(1);

        server.get("/", r -> {
            requests.add(r);
            return Response.ok("ok");
        });
        given().header("X-Test", "test", "test2").when().get("/")
                .then().assertThat().statusCode(200).and().body(equalTo("ok"));

        final Headers headers = requests.get(0).headers;
        final List<String> values = headers.get("X-Test");
        assertThat(values, CoreMatchers.hasItems("test", "test2"));
    }

}
