package mm.lambdawebserver.it;

import com.jayway.restassured.RestAssured;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.impl.servlet.ServletRunner;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

public class ServletTest {

    @Test
    public void servletTest() throws Exception {

        RestAssured.port = 7780;

        final LambdaHTTPServer server = ServletRunner.bind(7780);

        server.get("/", r -> Response.ok("ok"));

        get("/").then().statusCode(200).and().body(equalTo("ok"));
    }

}
