package mm.lambdawebserver.it;

import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import org.junit.Test;

import java.util.Optional;
import java.util.function.Function;

import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class RoutesTest extends AbstractServerTest{

    public RoutesTest(Function<Integer, LambdaHTTPServer> serverBuilder){
        super(serverBuilder);
    }

    @Test
    public void fragmentGetsApplied(){
        server.get("/*", r -> Response.ok(r.fragment.get()));
        get("/something").then().statusCode(200).and().body(equalTo("something"));
    }

    @Test
    public void fragmentCanBeEmpty(){
        server.get("/*", r -> Response.ok(r.fragment.orElse("ok")));
        get("/").then().statusCode(200).and().body(equalTo("ok"));
    }

    @Test
    public void fragmentsAndNo(){
        server.get("/test/*", r -> Response.ok(r.fragment.get()));
        server.get("/", r -> Response.ok("fail"));
        get("/test/something").then().statusCode(200).and().body(equalTo("something"));
    }

    @Test
    public void fragmentsDontContainQuery(){
        final StringBuilder query = new StringBuilder();
        server.get("/*", r -> {
            query.append(r.params.get("query"));
            return Response.ok(r.fragment.get());
        });
        get("/something?query=1").then().statusCode(200).and().body(equalTo("something"));
        assertThat(query.toString(), equalTo("1"));
    }

    @Test
    public void longestMatch(){
        server.get("/so*", r -> Response.error("fail1"));
        server.get("/some*", r -> Response.ok(r.fragment.get()));
        server.get("/s*", r -> Response.error("fail2"));

        get("/something").then().body(equalTo("thing"));
    }

    @Test
    public void exactMatchFirst(){
        server.get("/so*", r -> Response.error("fail"));
        server.get("/something", r -> Response.ok("ok"));
        server.get("/s*", r -> Response.error("fail"));

        get("/something").then().statusCode(200).and().body(equalTo("ok"));
    }

    @Test
    public void exactBeforeWildcard(){
        server.get("/some*", r -> Response.error("fail"));
        server.get("/some", r -> Response.ok("ok"));
        server.get("/som*", r -> Response.error("fail"));

        get("/some").then().statusCode(200).and().body(equalTo("ok"));
    }

    @Test
    public void fragmentsOrderOverwrite(){
        server.get("/*", r -> Response.ok("fail"));
        server.get("/something", r ->  Response.ok("ok"));
        server.get("/*", r -> Response.error("fail"));

        get("/something").then().statusCode(200).and().body(equalTo("ok"));
    }

    @Test
    public void filterCanHaveWildCards(){
        server.before("/*", r -> Optional.of(Response.ok(r.fragment.get())));
        server.get("/get*", r -> Response.error("hidden"));
        get("/getsomething").then().statusCode(200).and().body(equalTo("something"));
    }

    @Test
    public void canOverwriteRoute(){
        server.get("/", r -> Response.ok("first"));
        server.get("/", r -> Response.ok("second"));
        get("/").then().assertThat().content(equalTo("second"));
    }

    @Test
    public void forManual(){
        server.get("/p*", r -> Response.ok("4"));
        server.get("/p", r -> Response.ok("3"));
        server.get("/*", r -> Response.ok("2"));
        server.get("/", r -> Response.ok("1"));

        get("/").then().assertThat().body(equalTo("1"));
        get("/a").then().assertThat().body(equalTo("2"));
        get("/p").then().assertThat().body(equalTo("3"));
        get("/pp").then().assertThat().body(equalTo("4"));
    }
}
