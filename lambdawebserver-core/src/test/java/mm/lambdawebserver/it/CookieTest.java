package mm.lambdawebserver.it;

import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpCookie;
import java.util.function.Function;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class CookieTest extends AbstractServerTest {

    public CookieTest(Function<Integer, LambdaHTTPServer> serverBuilder) {
        super(serverBuilder);
    }

    @Test
    public void canSetCookie() throws IOException {
        final HttpCookie cookie = new HttpCookie("lambda", "value");
        cookie.setVersion(0);
        server.get("/", r -> Response.ok("ok").withCookie(cookie));
        get("/").then().assertThat().cookie("lambda", equalTo("value"));
    }


    @Test
    public void canReadCookie() throws IOException {
        server.get("/", r -> {
            final String cookie = r.cookieValue("lambda").orElse("NO COOKIE");
            return Response.ok(cookie);
        });
        given().cookie("lambda", "value").get("/").then().assertThat().body(equalTo("value"));
    }

}