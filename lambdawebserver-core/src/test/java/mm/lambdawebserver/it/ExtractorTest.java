package mm.lambdawebserver.it;


import mm.lambdawebserver.api.*;
import org.junit.Test;

import java.util.Optional;
import java.util.function.Function;

import static com.jayway.restassured.RestAssured.*;
import static mm.lambdawebserver.api.Extractor.*;
import static org.hamcrest.Matchers.equalTo;

public class ExtractorTest extends AbstractServerTest {

    public ExtractorTest(Function<Integer, LambdaHTTPServer> serverBuilder) {
        super(serverBuilder);
    }

    @Test
    public void withOutExtractors(){
        server.get("/*", r -> r.fragment.flatMap(fragment -> {
            try {
                return Optional.of(Integer.parseInt(fragment));
            } catch (NumberFormatException e) {
                return Optional.empty();
            }
        }).map(i -> Response.ok("got Integer " + i)).orElse(Response.error("failed to parse Integer")));
    }

    @Test
    public void one(){

        server.get("/*", extract(INT, i -> r -> Response.ok(i + 1)));
        get("/1").then().assertThat().statusCode(200).and().body(equalTo("2"));
        get("/").then().assertThat().statusCode(400);

    }

    @Test
    public void two(){

        server.post("/*", extract2(INT, BODY_INT, i -> j -> r -> Response.ok(i + j + "")));
        given().body("1").when().post("/1").then().assertThat().statusCode(200).and().body(equalTo("2"));

        post("/1").then().assertThat().statusCode(400);
        given().body("1").when().post("/").then().assertThat().statusCode(400);

    }

    @Test
    public void json(){

        server.post("/*", extract(JSON(Data.class), data -> r -> Response.ok(data)));
        given().body(new Data()).when().post("/1").then().assertThat().statusCode(200).and().body("data", equalTo("data"));

        post("/1").then().assertThat().statusCode(400);
    }

    @Test
    public void canExtractLong(){
        server.get("/*", extract(LONG, i -> r -> Response.ok(i)));

        get("/" + (Long.MAX_VALUE)).then().assertThat().statusCode(200).and().body(equalTo(Long.MAX_VALUE + ""));
        get("/").then().assertThat().statusCode(400);

        get("/a").then().assertThat().statusCode(400);

    }

    @Test
    public void canExtractFormUrlEncoded(){
        server.post("/", extract(BODY_URL_ENCODED, map -> r -> Response.ok(map.get("data"))));

        given().formParam("data","1").post("/").then().assertThat().body(equalTo("1")).and().statusCode(200);
    }


}
