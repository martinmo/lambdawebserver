package mm.lambdawebserver.it;

import com.jayway.restassured.http.ContentType;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.api.Wrapper;
import org.junit.Test;

import java.util.Optional;
import java.util.function.Function;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class WrapperTest extends AbstractServerTest {

    public WrapperTest(Function<Integer, LambdaHTTPServer> serverBuilder) {
        super(serverBuilder);
    }

    @Test
    public void canIntercept() {

        server.around("/i", h -> r -> Response.ok("intercepted"));

        get("/i").then().assertThat().statusCode(200).and().body(equalTo("intercepted"));
    }

    @Test
    public void canMutateHeader() {

        server.around("/i", h -> r -> {
            final String before = r.headers.getFirst("X-Before").get();
            final Response response = h.apply(r);
            return response.withHeader("X-After", before);
        });

        server.get("/i", r -> Response.ok("ok"));

        given().header("X-Before", "assert").when().get("/i").then().assertThat()
                .statusCode(200)
                .and().header("X-After", "assert")
                .and().body(equalTo("ok"));
    }

    @Test
    public void canMutateFragment() {

        final String fragment = "frag";

        server.around("/i*", h -> r -> h.apply(r.withPathFragment(Optional.of(fragment))));

        server.get("/ii*", r -> Response.ok(r.fragment.get()));

        get("/iii").then().assertThat()
                .statusCode(200)
                .and().body(equalTo(fragment));
    }

    @Test
    public void getsFragmentOfRequestHandler() {
        server.around("/i*", h -> r -> Response.ok(r.fragment.get()));
        server.get("/ii*", r -> {
            assert (r.fragment.get().equals("i"));
            return Response.ok("hidden");
        });
        get("/iii").then().assertThat().statusCode(200).and().body(equalTo("i"));
    }

    @Test
    public void getsNoFragmentOn404() {
        server.around("/*", h -> r -> Response.ok(r.fragment.map(f -> "fragment").orElse("no fragment")));
        get("/i").then().assertThat().statusCode(200).and().body(equalTo("no fragment"));
    }


    @Test
    public void multiple() {

        server.around("/i*", h -> r -> {
            final Response response = h.apply(r);
            return response.withHeader("X-After1", "1");
        });

        server.around("/ii", h -> r -> {
            final Response response = h.apply(r);
            return response.withHeader("X-After2", "2");
        });

        server.get("/ii", r -> Response.ok("ok"));

        get("/ii").then().assertThat()
                .statusCode(200)
                .and().header("X-After1", "1")
                .and().header("X-After2", "2")
                .and().body(equalTo("ok"));
    }

    @Test
    public void ordering() {

        server.around("/i", h -> r -> {
            final Response response = h.apply(r);
            return response.withHeader("X-X", response.headers.getFirst("X-X").orElse("") + "1");
        });

        server.around("/i", h -> r -> {
            final Response response = h.apply(r);
            return response.withHeader("X-X", response.headers.getFirst("X-X").orElse("") + "2");
        });

        server.get("/*", r -> Response.ok("ok"));

        get("/i").then().assertThat()
                .statusCode(200)
                .and().header("X-X", "12")
                .and().body(equalTo("ok"));
    }

    @Test
    public void requestHandlerFragmentNotChanged() {
        server.around("/*", Wrapper.TIMING);
        server.get("/test/*", r -> Response.ok(r.fragment.get()));
        get("/test/1").then().assertThat().body(equalTo("1")).and().contentType(ContentType.TEXT);
    }

    @Test
    public void canGZIP(){
        server.around("/*", Wrapper.GZIP);

        server.get("/",  r -> Response.ok("ok"));

        given().header("Accept-Encoding","gzip").get("/").then().assertThat().header("Content-Encoding", equalTo("gzip"));
    }

}
