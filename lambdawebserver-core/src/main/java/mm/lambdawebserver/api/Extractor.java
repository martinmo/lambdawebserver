package mm.lambdawebserver.api;

import com.google.common.collect.ImmutableMap;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@FunctionalInterface
public interface Extractor<T> {

    Optional<T> tryExtract(final Request request);

    static <T> RequestHandler extract(Extractor<T> extractor, Function<T, RequestHandler> handler) {
        return r -> extractor.tryExtract(r).map(extractedValue -> handler.apply(extractedValue).applySafe(r)).orElse(Response.code(400));
    }

    static <T, K> RequestHandler extract2(Extractor<T> extractor1, Extractor<K> extractor2, Function<T, Function<K, RequestHandler>> handler) {
        return r -> {
            final Optional<T> param1 = extractor1.tryExtract(r);
            final Optional<K> param2 = extractor2.tryExtract(r);
            if (param1.isPresent() && param2.isPresent()) {
                return handler.apply(param1.get()).apply(param2.get()).apply(r);
            } else {
                return Response.code(400);
            }
        };
    }

    public static Extractor<Integer> INT =
            (request) -> request.fragment.flatMap(
                    f -> tryF(s -> Integer.parseInt(s), f)
            );


    public static Extractor<Integer> BODY_INT =
            (request) -> tryF(Integer::parseInt, request.bodyAsString());

    public static Extractor<Long> LONG =
            (request) -> request.fragment.flatMap(
                    (String f) -> tryF(Long::parseLong, f)
            );

    public static Extractor<String> STRING =
            (request) -> request.fragment.filter(s -> !"".equals(s.trim()));


    public static <T> Extractor<T> JSON(Class<T> clazz) {
        return r -> r.getData(clazz);
    }

    public static <A, B> Optional<B> tryF(Function<A, B> f, final A from) {
        try {
            return Optional.ofNullable(f.apply(from));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public static Extractor<Map<String,String>> BODY_URL_ENCODED =
            r -> {
                try {
                    final String body = r.bodyAsString();
                    String[] pairs = body.split("\\&");

                    final ImmutableMap.Builder<String, String> map = ImmutableMap.<String, String>builder();

                    for (int i = 0; i < pairs.length; i++) {
                        String[] fields = pairs[i].split("=");
                        String name = URLDecoder.decode(fields[0], "UTF-8");
                        String value = URLDecoder.decode(fields[1], "UTF-8");
                        map.put(name, value);
                    }

                    return Optional.of(map.build());
                } catch (Exception e) {
                    e.printStackTrace();
                    return Optional.empty();
                }
            };
}
