package mm.lambdawebserver.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.MimetypesFileTypeMap;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpCookie;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Optional;
import java.util.zip.GZIPOutputStream;

public class Response {

    private final static ObjectMapper mapper = new ObjectMapper();

    private final static MimetypesFileTypeMap mimeTypes = new MimetypesFileTypeMap();

    public final BodyContent body;
    public final int code;
    public final Headers headers;

    Response(final BodyContent body, final int code, Headers headers) {
        this.body = body;
        this.code = code;
        this.headers = headers;
    }

    Response(final BodyContent body, final int code) {
        this.body = body;
        this.code = code;
        this.headers = new Headers().contentTypeText();
    }

    public static Response ok(final String message) {
        return new Response(InMemoryContent.of(message), 200);
    }

    public static Response bytes(byte[] bytes) {
        return new Response((InMemoryContent.of(bytes)), 200);
    }

    public static Response serverFile(final String path, final Request request) {
        return request.fragment.map(fragment -> Response.file(path, fragment)).orElse(Response.error("invalid"));
    }

    public static Response ok(final Object object) {
        try {
            final BodyContent bytes = new InMemoryContent(mapper.writeValueAsBytes(object));
            return new Response(bytes, 200).asJSON();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> Response mayBeOk(final Optional<T> optional) {
        if (optional.isPresent()) {
            return ok(optional.get());
        } else {
            return notFound();
        }
    }

    public static Response code(final int code) {
        return new Response(InMemoryContent.empty(), code);
    }

    public static Response notFound() {
        return new Response(InMemoryContent.of("not found".getBytes()), 404);
    }

    public static Response notFound(final String what) {
        return new Response(InMemoryContent.of((what + " not found").getBytes()), 404);
    }

    public static Response error(final String cause) {
        return new Response(InMemoryContent.of(cause), 500);
    }

    public static Response movedPermanently(final String target) {
        return Response.code(301).withHeader("Location", target);
    }

    /**
     * redirect after post
     * @param target
     * @return
     */
    public static Response seeOther(final String target) {
        return Response.code(303).withHeader("Location", target);
    }

    public static Response temporaryRedirect(final String target) {
        return Response.code(302).withHeader("Location", target);
    }

    public static Response file(final String dir, final String path) {
        final Optional<FileContent> contentOptional = FileContent.of(dir, path);
        if (contentOptional.isPresent()) {

            final String contentType = contentOptional.get().guessContentType().orElse("application/octet-stream");
            return new Response(contentOptional.get(), 200).as(contentType);
        } else {
            return Response.notFound(path);
        }
    }

    public static Response externalFile(final String dir, final String path) {
        final Optional<FileContent> contentOptional = FileContent.ofExternal(dir, path);
        if (contentOptional.isPresent()) {

            final String contentType = contentOptional.get().guessContentType().orElse("application/octet-stream");
            return new Response(contentOptional.get(), 200).as(contentType);
        } else {
            return Response.notFound(path);
        }
    }

    public static Response file(final String path) {
        return file("/", path);
    }

    public Response cacheFor(Duration duration) {
        final String value = String.format("max-age=%s, must-revalidate", duration.getSeconds());
        return withHeader("Cache-Control", value);
    }

    public Response changeCode(final int code) {
        return new Response(body, code, headers);
    }

    public Response withHeader(final String name, final String value) {
        final Headers newHeaders = headers.putOrReplace(name, value);
        return new Response(body, code, newHeaders);
    }

    public Response withCookie(final HttpCookie cookie){
        return withHeader("Set-Cookie", cookie.toString());
    }

    public Response as(final String contentType) {
        final Headers newHeaders = headers.putOrReplace("Content-Type", contentType);
        return new Response(body, code, newHeaders);
    }

    public Response asJSON() {
        return as("application/json; charset=utf-8");
    }

    public Response asHTML() {
        return as("text/html; charset=utf-8");
    }

    public Response gzip() {
        final Headers newHeaders = headers.put("Content-Encoding", "gzip");
        return new Response(new GzipContent(body), code, newHeaders);
    }

    public interface BodyContent {
        long size();

        InputStream stream();
    }

    public static class GzipContent implements BodyContent {

        private final byte[] zipped;

        public GzipContent(BodyContent wrapped) {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            try(GZIPOutputStream gzipOutput = new GZIPOutputStream(bytesOutput)) {
                IOUtils.copy(wrapped.stream(), gzipOutput);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            zipped = bytesOutput.toByteArray();
        }

        @Override
        public long size() {
            return zipped.length;
        }

        @Override
        public InputStream stream() {
            return new ByteArrayInputStream(zipped);
        }

    }

    private static class InMemoryContent implements BodyContent {
        private final byte[] bytes;

        private InMemoryContent(final byte[] bytes) {
            this.bytes = bytes;
        }

        public static InMemoryContent of(final byte[] bytes) {
            return new InMemoryContent(bytes);
        }

        public static InMemoryContent of(final String text) {
            return new InMemoryContent(text.getBytes());
        }

        public static InMemoryContent empty() {
            return new InMemoryContent(new byte[]{});
        }

        @Override
        public long size() {
            return bytes.length;
        }

        @Override
        public InputStream stream() {
            return new ByteArrayInputStream(bytes);
        }

        @Override
        public String toString() {
            return "Byte[" + new String(bytes) + "]";
        }
    }

    private static class FileContent implements BodyContent {

        private final static Logger LOG = LoggerFactory.getLogger(FileContent.class);

        private final Path path;

        private FileContent(final Path path) {
            this.path = path;
        }

        public static Optional<FileContent> of(final String dirString, final String fileNameString) {

            try {
                final URL dirUrl = Response.class.getResource(dirString);
                if (dirUrl == null) {
                    LOG.error("directory {}  not found", dirString);
                    return Optional.empty();
                }
                final Path dir = Paths.get(dirUrl.toURI());
                final Path filePath = dir.resolve(fileNameString);
                if (Files.exists(filePath) && Files.isReadable(filePath)) {
                    return Optional.of(new FileContent(filePath));
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return Optional.empty();
        }

        public static Optional<FileContent> ofExternal(final String dirString, final String fileNameString) {

            final Path dir = Paths.get(dirString);

            if (!Files.isReadable(dir)) {
                LOG.error(dir + " not readable");
                return Optional.empty();
            }

            final Path filePath = dir.resolve(fileNameString);
            if (Files.exists(filePath) && Files.isReadable(filePath) && Files.isRegularFile(filePath)) {
                return Optional.of(new FileContent(filePath));
            }

            return Optional.empty();
        }

        @Override
        public long size() {
            try {
                return Files.size(path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public InputStream stream() {
            try {
                return Files.newInputStream(path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public Optional<String> guessContentType() {
            return Optional.ofNullable(mimeTypes.getContentType(path.getFileName().toString()));
        }

        @Override
        public String toString() {
            return "File(" + path + ")";
        }
    }

    @Override
    public String toString() {
        return "Resp[" + code + " " + headers.toString() + " content: " + body + "]";
    }
}
