package mm.lambdawebserver.api;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpCookie;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Request {

    private final static ObjectMapper mapper = new ObjectMapper();
    static {
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
    }

    public final URI uri;
    public final InputStream body;
    public final Map<String, String> params;
    public final Method method;
    public final Headers headers;
    public final Optional<String> fragment;

    public Request(final Method method, final URI uri, final InputStream body, final Headers headers, final Optional<String> fragment) {
        this.method = method;
        this.uri = uri;
        this.body = body;
        this.headers = headers;
        this.fragment = fragment;
        Map<String, String> params = new HashMap<>();
        if (uri.getQuery() != null && !"".equals(uri.getQuery())) {
            params.putAll(Splitter.on('&').trimResults().withKeyValueSeparator('=').split(uri.getQuery()));
        }
        this.params = (params);
    }

    public Request(final Method method, final URI uri, InputStream body, final Headers headers, final Optional<String> fragment, final Map<String, String> params) {
        this.method = method;
        this.uri = uri;
        this.body = body;
        this.headers = headers;
        this.fragment = fragment;
        if(params instanceof ImmutableMap) {
            this.params = params;
        }else {
            this.params = Collections.unmodifiableMap(params);
        }
    }

    public <T> Optional<T> getData(Class<T> clazz) {
        try {
            final T t = mapper.readValue(body, clazz);
            return Optional.of(t);
        } catch (IOException e) {
            LoggerFactory.getLogger(Request.class).warn("failed to parse class {} because of {}", clazz, e.getMessage());
            return Optional.empty();
        }
    }

    public String bodyAsString() {
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(body, writer, "UTF-8");
            return writer.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Optional<String> getParam(final String name) {
        return Optional.ofNullable(params.get(name));
    }

    public Request withPathFragment(final Optional<String> pathFragment) {
        return new Request(method, uri, body, headers, pathFragment, params);
    }

    @Override
    public String toString() {
        return "Req[" + method + " " + uri + " frag(" + fragment.orElse("<NONE>") + ") " + headers + "]";
    }

    public Optional<HttpCookie> cookie(final String name){
        return headers.getFirst("Cookie").flatMap(h ->
                        HttpCookie.parse(h).stream().filter(c -> c.getName().equals(name)).findFirst()
        );
    }

    public Optional<String> cookieValue(final String name){
        return cookie(name).map(c -> c.getValue());
    }
}
