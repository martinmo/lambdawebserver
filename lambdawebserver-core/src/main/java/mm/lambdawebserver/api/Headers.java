package mm.lambdawebserver.api;

import java.util.*;
import java.util.stream.Collectors;

// TODO make immutable
public class Headers {

    private final Map<String, List<String>> headers;

    private Headers(Map<String, List<String>> map){
        this.headers =  map;
    }
    public Headers(){
        this.headers = new HashMap<>();
    }


    public Optional<String> getFirst(final String name){
        final List<String> list = headers.get(name);
        if(list != null && list.size() >0){
            return Optional.of(list.get(0));
        }else {
            return Optional.empty();
        }
    }

    public Headers put(final String name, final String value){
        headers.putIfAbsent(name, new LinkedList<>());
        headers.get(name).add(value);
        return this;
    }

    public Headers putOrReplace(final String name, final String value){
        headers.put(name, new LinkedList<>());
        headers.get(name).add(value);
        return this;
    }

    public List<String> get(final String name){
        return headers.getOrDefault(name, new LinkedList<>());
    }


    public Headers contentTypeJSON(){
        return put("Content-Type", "application/json");
    }

    public Headers contentTypeText(){
        return put("Content-Type", "text/plain");
    }

    public static Headers fromMap(Map<String, List<String>> map){
        return new Headers(map);
    }

    public Map<String, List<String>> getMap(){return headers;}

    @Override
    public String toString() {
        return "H{" + headers.entrySet().stream().map( e -> e.getKey() + ": [" + e.getValue().stream().collect(Collectors.joining(";"))+ "]").collect(Collectors.joining(";")) + "}";
    }
}
