package mm.lambdawebserver.api;

import org.slf4j.LoggerFactory;

import java.util.Base64;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

@FunctionalInterface
public interface BeforeFilter {

    // may or may not finish the request
    Optional<Response> apply(Request request);

    static BeforeFilter LOG = (r) -> {
        LoggerFactory.getLogger(BeforeFilter.class).info("[{}] {}", r.method, r.uri);
        return Optional.empty();
    };

    static BeforeFilter auth(final String realm, final BiPredicate<String, String> credentialsChecker) {
        final Predicate<String> validateAuthHeaderValue = base -> {
            final String base64 = base.replace("Basic ", "");
            final String userNamePassword = new String(Base64.getDecoder().decode(base64));
            final String[] split = userNamePassword.split(":");
            String username = split[0];
            String password = split[1];
            return credentialsChecker.test(username, password);
        };

        final BeforeFilter auth = r -> {
            final Optional<String> authHeader = r.headers.getFirst("Authorization");
            if (authHeader.isPresent()) {
                try {
                    final boolean valid = validateAuthHeaderValue.test(authHeader.get());
                    if (valid) {
                        return Optional.empty();
                    }
                } catch (Exception e) {
                    return Optional.of(Response.error("error in basic auth"));
                }
            }
            return Optional.of(Response.code(401).withHeader("WWW-Authenticate", " Basic realm=\"" + realm + "\""));
        };
        return auth;
    }
}
