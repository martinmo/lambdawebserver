package mm.lambdawebserver.api;

@FunctionalInterface
public interface RequestHandler{

    Response apply(Request request) throws Exception;

    default Response applySafe(Request request){
        try {
            return apply(request);
        } catch (Exception e) {
            if(e instanceof RuntimeException){
                throw (RuntimeException)e;
            }else {
                throw new RuntimeException(e);
            }
        }
    }

    static RequestHandler NOT_FOUND = r -> Response.notFound();

    static RequestHandler serveFiles(final String dir){
      return r -> r.fragment.map(f -> Response.file(dir, f)).orElse(Response.notFound());
    }
}
