package mm.lambdawebserver.api;

import java.util.function.Function;

public interface LambdaHTTPServer {
    void get(String path, RequestHandler f);

    void post(String path, RequestHandler f);

    void delete(String path, RequestHandler f);

    void put(String path, RequestHandler f);

    void before(String path, BeforeFilter filter);

    void handle(Method method, String path, RequestHandler handler);

    void around(String path, Wrapper wrapper);

    void onError(Function<Throwable, RequestHandler> catcher);

    LambdaHTTPServer context(String path);

    void stop();

    String contextPath();
}
