package mm.lambdawebserver.api;

import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Optional;

@FunctionalInterface
public interface Wrapper {

    RequestHandler around(RequestHandler handler);

    static Wrapper TIMING = handler -> {
        final long begin = System.currentTimeMillis();

        RequestHandler newHandler = r -> {
            final Response response = handler.apply(r);
            final long end = System.currentTimeMillis();
            LoggerFactory.getLogger(Wrapper.class).info(new Date() + " [" + r.method + "] " + r.uri + " -> " + response.code + ", " + (end - begin) + "ms");
            return response;
        };

        return newHandler;
    };

    static Wrapper GZIP = handler -> {
        RequestHandler newHandler = r -> {
            final Response response = handler.apply(r);

            final Boolean canGzip = r.headers.getFirst("Accept-Encoding").map(h -> h.indexOf("gzip") != -1).orElse(false);

            if(!canGzip){
                return response;
            }

            final Optional<String> zipable = response.headers.getFirst("Content-Type")
                    .filter(contenttType -> contenttType.startsWith("text") || contenttType.contains("application/json"));

            if (zipable.isPresent()) {
                return response.gzip();
            } else {
                return response;
            }
        };
        return newHandler;
    };

}
