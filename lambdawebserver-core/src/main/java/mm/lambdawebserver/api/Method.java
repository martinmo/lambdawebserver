package mm.lambdawebserver.api;

public enum Method {
    GET, POST, PUT, DELETE, ANY;

    public boolean matches(Method other){
        return (this.equals(ANY) || other.equals(ANY)) || this.equals(other);
    }
}
