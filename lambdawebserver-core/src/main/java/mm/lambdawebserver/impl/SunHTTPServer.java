package mm.lambdawebserver.impl;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Method;
import mm.lambdawebserver.api.Request;
import mm.lambdawebserver.api.Response;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Optional;

public class SunHTTPServer {

    private final HttpServer server;
    private final ServerEngine engine = new ServerEngine();
    private final static int MAX_REQUEST_BODY_SIZE = (1024 * 2);

    SunHTTPServer(final HttpServer server) {
        this.server = server;
        engine.registerStopCallback(() -> {
            this.stop();
            return "sun http server stopped";
        });
        init();
    }

    public static LambdaHTTPServer bind(final int port, final String hostname) throws IOException {
        final HttpServer httpServer = HttpServer.create(new InetSocketAddress(hostname, port), 0);
        return new SunHTTPServer(httpServer).engine;
    }

    public static LambdaHTTPServer bind(final int port) throws IOException {
        return bind(port, "localhost");
    }

    private void init() {
        server.createContext("/", httpExchange -> {
            final Optional<Request> request = buildRequest(httpExchange);
            final Optional<Response> response = request.map(r -> engine.handleRequestResponse(r));
            if(response.isPresent()){
                writeResponse(httpExchange, response.get());
            }
        });
        server.start();
    }

    private Optional<Request> buildRequest(HttpExchange httpExchange) throws IOException {
        //final byte[] bytes = ByteStreams.toByteArray(ByteStreams.limit(httpExchange.getRequestBody(), MAX_REQUEST_BODY_SIZE + 1));

//        if(bytes.length > MAX_REQUEST_BODY_SIZE){
//            writeResponse(httpExchange, Response.error("body to large"));
//            return Optional.empty();
//        }

        final mm.lambdawebserver.api.Headers headers = mm.lambdawebserver.api.Headers.fromMap(httpExchange.getRequestHeaders());
        final Request request = new Request(Method.valueOf(httpExchange.getRequestMethod()), httpExchange.getRequestURI(), httpExchange.getRequestBody(), headers, Optional.<String>empty());
        return Optional.of(request);
    }

    private void writeResponse(HttpExchange httpExchange, Response response) throws IOException {
        httpExchange.getResponseHeaders().putAll(response.headers.getMap());
        httpExchange.sendResponseHeaders(response.code, response.body.size());
        final OutputStream outputStream = httpExchange.getResponseBody();
        IOUtils.copy(response.body.stream(), outputStream);
        outputStream.close();
        httpExchange.close();
    }

    public void stop() {
        server.stop(0);
    }
}
