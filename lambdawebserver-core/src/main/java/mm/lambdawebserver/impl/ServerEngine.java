package mm.lambdawebserver.impl;

import mm.lambdawebserver.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.SortedSet;
import java.util.function.Function;
import java.util.function.Supplier;

public class ServerEngine extends AbstractServerEngine implements LambdaHTTPServer{

    private final static Logger LOG = LoggerFactory.getLogger(ServerEngine.class);

    private final Routing routing;
    private final String contextPath;

    private Function<Throwable, RequestHandler> errorHandler = (e) ->
            (request) -> {
                e.printStackTrace();
                return Response.error(e.getMessage());
            };

    public ServerEngine() {
        this.contextPath = "";
        this.routing = new Routing();
    }

    public ServerEngine(String contextPath, Function<Throwable, RequestHandler> errorHandler, Supplier<String> stopper, Routing routing) {
        this.contextPath = contextPath;
        this.errorHandler = errorHandler;
        this.stopper = stopper;
        this.routing = routing;
    }

    @Override
    public void around(String path, Wrapper wrapper) {
        routing.register(Method.ANY, contextPath + path, wrapper);
    }

    @Override
    public void handle(final Method method, final String path, final RequestHandler f) {
        routing.register(method, contextPath + path, f);
    }

    @Override
    public void onError(Function<Throwable, RequestHandler> catcher) {
        errorHandler = catcher;
    }

    @Override
    public LambdaHTTPServer context(String path) {
        return new ServerEngine(contextPath + path, errorHandler, stopper, routing);
    }

    public Response handleRequestResponse(Request request) {
        try {
            return applyHandler(request);
        } catch (Exception e) {
            return errorHandler.apply(e).applySafe(request);
        }
    }

    private Response applyHandler(final Request request) {

        final Routing.RoutingResult<RequestHandler> routingResult = routing.findRequestHandler(request);

        LOG.debug("before apply {}", routingResult.request);
        final RequestHandler wrappedHandler = applyWrappers(routingResult.request, routingResult.handler);

        final Response response = wrappedHandler.applySafe(routingResult.request);
        LOG.debug("after wrappers {}", response);
        return response;
    }

    private RequestHandler applyWrappers(final Request request, RequestHandler handler) {
        final SortedSet<Routing.RoutingResult<Wrapper>> routingResults = routing.findWrappers(request);

        RequestHandler result = handler;

        for (Routing.RoutingResult<Wrapper> routingResult : routingResults) {
            result = routingResult.handler.around(result);
        }
        return result;
    }

    Supplier<String> stopper = () -> "no stopper";
    public void registerStopCallback(Supplier<String> stopper){
        this.stopper = stopper;
    }

    @Override
    public void stop() {
        final String message = stopper.get();
        LOG.info("shut down message: {}", message);
    }

    @Override
    public String contextPath() {
        return contextPath;
    }
}
