package mm.lambdawebserver.impl;

import mm.lambdawebserver.api.Method;
import mm.lambdawebserver.api.Request;
import mm.lambdawebserver.api.RequestHandler;
import mm.lambdawebserver.api.Wrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class Routing {

    private final static Logger LOG = LoggerFactory.getLogger(Routing.class);

    private static class Route implements Comparable<Route> {
        final String path;
        final boolean wildcard;
        final Method method;

        private Route(String path, boolean wildcard, Method method) {
            this.path = path;
            this.wildcard = wildcard;
            this.method = method;
        }

        public static Route from(final String rawPath, final Method method) {
            final boolean wildcard;
            final String path;
            if (rawPath.endsWith("*")) {
                wildcard = true;
                path = rawPath.substring(0, rawPath.length() - 1);
            } else {
                wildcard = false;
                path = rawPath;
            }
            return new Route(path, wildcard, method);
        }

        public boolean matches(final Method method, final String rawPath) {
            if (!this.method.matches(method)) {
                return false;
            }
            if (wildcard) {
                return rawPath.startsWith(path);
            } else {
                return path.equals(rawPath);
            }
        }

        public Optional<String> extractFragment(final String rawPath) {
            final String frag = rawPath.replaceFirst(path, "");
            if ("".equals(frag)) {
                return Optional.empty();
            } else {
                return Optional.of(frag);
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Route route = (Route) o;

            if (wildcard != route.wildcard) return false;
            if (method != route.method) return false;
            if (!path.equals(route.path)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = path.hashCode();
            result = 31 * result + (wildcard ? 1 : 0);
            result = 31 * result + method.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "Route(" + method + " " + path + " (" + wildcard + "))";
        }

        @Override
        public int compareTo(Route o) {
            if(!this.wildcard && o.wildcard){
                return -1;
            }
            if(this.wildcard && !o.wildcard){
                return 1;
            }
            return Integer.valueOf(o.path.length()).compareTo(this.path.length()) ;
        }
    }

    public static class RoutingResult<T> implements Comparable<RoutingResult<?>> {
        public final Request request;
        public final Optional<Route> route;
        public final T handler;

        RoutingResult(final Request request, final T handler, final Optional<Route> route) {
            this.request = request;
            this.handler = handler;
            this.route = route;
        }

        public static <T> RoutingResult<T> of(final Request request, final T handler, final Route route) {
            return new RoutingResult<>(request, handler, Optional.of(route));
        }

        public static <T> RoutingResult<T> of(final Request request, final T handler) {
            return new RoutingResult<>(request, handler, Optional.empty());
        }

        @Override
        public int compareTo(RoutingResult<?> o) {
            if(this.route.isPresent() && !o.route.isPresent()){
                return 1;
            }
            if(!this.route.isPresent() && o.route.isPresent()){
                return -1;
            }
            final int routesOrder = o.route.get().compareTo(this.route.get());
            if(routesOrder == 0){
                return 1;
            }else {
                return -1 * routesOrder;
            }
        }
    }

    private final Map<Route, List<RequestHandler>> handlers = Collections.synchronizedMap(new LinkedHashMap<>());
    private final Map<Route, List<Wrapper>> wrappers = Collections.synchronizedMap(new LinkedHashMap<>());

    public void register(final Method method, final String rawPath, final RequestHandler handler) {
        final Route route = Route.from(rawPath, method);
        LOG.debug("register handler for {} from path {}", route, rawPath);
        handlers.put(route, new LinkedList<>());
        handlers.get(route).add(handler);
    }

    public void register(final Method method, final String rawPath, final Wrapper wrapper) {
        final Route route = Route.from(rawPath, method);
        LOG.debug("register wrapper for {} from path {}", route, rawPath);
        if (!wrappers.containsKey(route)) {
            wrappers.put(route, new LinkedList<>());
        }
        wrappers.get(route).add(wrapper);
    }

    public RoutingResult<RequestHandler> findRequestHandler(final Request request) {
        final SortedSet<RoutingResult<RequestHandler>> matchingHandlers = findMatchingHandlers(request, handlers);

        if (matchingHandlers.size() > 0) {
            return matchingHandlers.first();
        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug("handler for {} not found in {}", request, handlers.keySet());
            }
            return RoutingResult.of(request, RequestHandler.NOT_FOUND);
        }
    }

    public SortedSet<RoutingResult<Wrapper>> findWrappers(final Request request) {
        return findMatchingHandlers(request, wrappers);
    }

    private <H> SortedSet<RoutingResult<H>> findMatchingHandlers(final Request request, final Map<Route, List<H>> handlersMap) {
        final SortedSet<RoutingResult<H>> foundHandler = new TreeSet<>();
        for (Map.Entry<Route, List<H>> entry : handlersMap.entrySet()) {
            final Route route = entry.getKey();
            final List<H> handlers = entry.getValue();

            final List<RoutingResult<H>> matchedHandlers = matchRoute(route, request, handlers);
            foundHandler.addAll(matchedHandlers);
        }
        return foundHandler;
    }

    private <H> List<RoutingResult<H>> matchRoute(Route route, Request request, List<H> handlers) {
        final String rawPath = request.uri.getPath();

        if (route.matches(request.method, rawPath)) {
            Optional<String> fragment = route.extractFragment(rawPath);
            final Request withFragment = request.withPathFragment(fragment);

            return handlers.stream().map(h -> RoutingResult.of(withFragment, h, route)).collect(Collectors.toList());
        }
        return new LinkedList<>();
    }

}
