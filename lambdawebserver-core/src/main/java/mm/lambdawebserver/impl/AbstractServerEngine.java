package mm.lambdawebserver.impl;

import mm.lambdawebserver.api.*;

public abstract class AbstractServerEngine implements LambdaHTTPServer{

    @Override
    public void get(final String path, RequestHandler f) {
        handle(Method.GET, path, f);
    }

    @Override
    public void post(final String path, RequestHandler f) {
        handle(Method.POST, path, f);
    }

    @Override
    public void delete(final String path, RequestHandler f) {
        handle(Method.DELETE, path, f);
    }

    @Override
    public void put(final String path, RequestHandler f) {
        handle(Method.PUT, path, f);
    }

    @Override
    public void before(final String path, BeforeFilter filter) {
        around(path, (wrappedHandler) -> request -> filter.apply(request).orElse(wrappedHandler.apply(request)));
    }
}
