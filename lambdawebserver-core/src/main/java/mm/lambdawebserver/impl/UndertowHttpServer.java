package mm.lambdawebserver.impl;

import com.google.common.collect.Lists;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.GracefulShutdownHandler;
import io.undertow.server.handlers.RequestLimitingHandler;
import io.undertow.util.HeaderMap;
import io.undertow.util.HttpString;
import mm.lambdawebserver.api.*;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class UndertowHttpServer {

    private static final Logger LOG = LoggerFactory.getLogger(UndertowHttpServer.class);

    private final static int WORKER_THREADS = 10;
    private final static int MAX_CONCURRENT_REQUESTS = 100;
    private final static int MAX_REQUEST_BODY_SIZE = (1024 * 2);

    private Undertow server;
    private final ServerEngine engine = new ServerEngine();
    private final int port;
    private final String hostname;
    private GracefulShutdownHandler shutdownHandler;

    UndertowHttpServer(final int port, final String hostname) {
        this.port = port;
        this.hostname = hostname;
        init();
    }

    private void init() {

        final HttpHandler lambdaServerHandler = new HttpHandler() {                                                 //Default Handler
            @Override
            public void handleRequest(final HttpServerExchange exchange) throws Exception {
                exchange.setMaxEntitySize(MAX_REQUEST_BODY_SIZE);
                LOG.trace("undertow handler begin");
                if (exchange.isInIoThread()) {
                    exchange.dispatch(this);
                    return;
                }
                exchange.startBlocking();
                final Request request = buildRequest(exchange);
                final Response response = engine.handleRequestResponse(request);
                writeResponse(exchange, response);
                exchange.endExchange();
                LOG.trace("undertow handler end");
            }
        };

        final RequestLimitingHandler requestLimitingHandler = new RequestLimitingHandler(MAX_CONCURRENT_REQUESTS, lambdaServerHandler);

        shutdownHandler = new GracefulShutdownHandler(requestLimitingHandler);

        server = Undertow.builder()
                .addHttpListener(port, hostname)
                .setWorkerThreads(WORKER_THREADS)
                .setHandler(shutdownHandler)
                .build();
        server.start();

        engine.registerStopCallback(() -> {
            this.stop();
            return "stopped undertow server";
        });

        LOG.info("started undertow server on {}:{}", hostname, port);
    }

    public static LambdaHTTPServer bind(final int port) throws IOException {
        return new UndertowHttpServer(port, "localhost").engine;
    }

    public static LambdaHTTPServer bind(final int port, final String hostname) throws IOException {
        return new UndertowHttpServer(port, hostname).engine;
    }

    private static Request buildRequest(HttpServerExchange httpExchange) throws IOException, URISyntaxException {

        final Headers headers = buildHeaders(httpExchange);
        final String queryString = httpExchange.getQueryString();
        final String relativePath;
        if (queryString != null && !"".equals(queryString)) {
            relativePath = httpExchange.getRelativePath() + "?" + queryString;
        } else {
            relativePath = httpExchange.getRelativePath();
        }
        final String method = httpExchange.getRequestMethod().toString();

        // TODO check if this is the correct way to do it
//        final int requestSize = (int) httpExchange.getRequestContentLength();
//        final int bufferSize = requestSize != -1 ? requestSize : 0;
//        final byte[] bytes;
//        if (requestSize > 0) {
//            bytes = new byte[bufferSize];
//            try (InputStream inputStream = httpExchange.getInputStream()) {
//                IOUtils.readFully(inputStream, bytes);
//            }
//        } else {
//            bytes = new byte[0];
//        }
        return new Request(Method.valueOf(method), new URI(relativePath), httpExchange.getInputStream(), headers, Optional.<String>empty());
    }

    private static Headers buildHeaders(HttpServerExchange httpExchange) {
        final HeaderMap requestHeaders = httpExchange.getRequestHeaders();
        Map<String, List<String>> headers = new HashMap<>();
        for (HttpString name : requestHeaders.getHeaderNames()) {
            final Iterable<String> strings = requestHeaders.eachValue(name);
            final ArrayList<String> values = Lists.newArrayList(strings);

            headers.put(name.toString(), values);
        }
        return Headers.fromMap(headers);
    }

    private static void writeResponse(HttpServerExchange httpExchange, Response response) throws IOException {
        httpExchange.setResponseCode(response.code);
        final HeaderMap responseHeaders = httpExchange.getResponseHeaders();
        for (Map.Entry<String, List<String>> entry : response.headers.getMap().entrySet()) {
            final HttpString name = HttpString.tryFromString(entry.getKey());
            responseHeaders.addAll(name, entry.getValue());
        }
        httpExchange.setResponseContentLength(response.body.size());
        final OutputStream outputStream = httpExchange.getOutputStream();
        try(InputStream in = response.body.stream()) {
            IOUtils.copy(in, outputStream);
        }
        outputStream.close();
    }

    public void stop() {
        shutdownHandler.shutdown();
        try {
            shutdownHandler.awaitShutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        server.stop();
    }
}
