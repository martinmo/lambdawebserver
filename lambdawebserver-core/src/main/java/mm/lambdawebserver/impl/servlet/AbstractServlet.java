package mm.lambdawebserver.impl.servlet;

import com.google.common.io.ByteStreams;
import mm.lambdawebserver.api.Headers;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Method;
import mm.lambdawebserver.api.Request;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.impl.ServerEngine;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractServlet implements Servlet {

    private String contextPath;
    private ServerEngine engine;

    public abstract void configure(LambdaHTTPServer server);

    @Override
    public void init(ServletConfig config) throws ServletException {
        contextPath = config.getServletContext().getContextPath();
        engine = new ServerEngine();

        engine.registerStopCallback(() -> {
            throw new RuntimeException("servlet cannot be stopped!!");
        });
        engine = (ServerEngine) engine.context(contextPath);
        configure(engine);
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        final HttpServletRequest servletRequest = (HttpServletRequest) req;
        final HttpServletResponse servletResponse = (HttpServletResponse) res;

        try {
            final Request request = toRequest(servletRequest);
            final Response response = engine.handleRequestResponse(request);
            applyResponse(response, servletResponse);
        } catch (URISyntaxException e) {
            throw new ServletException(e);
        }
    }

    private Request toRequest(HttpServletRequest servletRequest) throws URISyntaxException, IOException {
        final Method method = Method.valueOf(servletRequest.getMethod());

        final URI uri;
        final String queryString = servletRequest.getQueryString();
        if (queryString != null && !"".equals(queryString)) {
            uri = new URI(servletRequest.getRequestURL().toString() + "?" + queryString);
        } else {
            uri = new URI(servletRequest.getRequestURL().toString());
        }

        final InputStream body = servletRequest.getInputStream();
        final Headers headers = new Headers();

        final Enumeration<String> headerNames = servletRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            final String name = headerNames.nextElement();
            final Enumeration<String> headersValues = servletRequest.getHeaders(name);
            while (headersValues.hasMoreElements()) {
                final String value = headersValues.nextElement();
                headers.put(name, value);
            }

        }

        final Optional<String> fragment = Optional.empty();

        return new Request(method, uri, body, headers, fragment);
    }

    public String getContextPath() {
        return contextPath;
    }

    public ServerEngine getEngine() {
        return engine;
    }

    private void applyResponse(Response response, HttpServletResponse servletResponse) throws IOException {
        servletResponse.setStatus(response.code);
        for (Map.Entry<String, List<String>> entry : response.headers.getMap().entrySet()) {
            final String name = entry.getKey();
            for (String value : entry.getValue()) {
                servletResponse.setHeader(name, value);
            }
        }

        try(InputStream inputStream = response.body.stream()) {
            final ServletOutputStream outputStream = servletResponse.getOutputStream();
            ByteStreams.copy(inputStream, outputStream);
        }
    }


    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {
    }
}
