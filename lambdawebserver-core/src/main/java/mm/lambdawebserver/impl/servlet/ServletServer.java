package mm.lambdawebserver.impl.servlet;

import mm.lambdawebserver.api.LambdaHTTPServer;

public class ServletServer extends AbstractServlet {

    LambdaHTTPServer lambdaHTTPServer;

    @Override
    public void configure(LambdaHTTPServer server) {
        this.lambdaHTTPServer = server;
    }

    public LambdaHTTPServer getLambdaHTTPServer() {
        return lambdaHTTPServer;
    }
}
