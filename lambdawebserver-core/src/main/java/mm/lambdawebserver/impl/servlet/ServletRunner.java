package mm.lambdawebserver.impl.servlet;

import mm.lambdawebserver.api.LambdaHTTPServer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.Servlet;

public class ServletRunner {

    private final Server server;

    private ServletRunner(int port) {
        server = new Server(port);
    }

    public void start(Servlet servlet) throws Exception {

        ServletContextHandler context = new ServletContextHandler();

        final ServletHolder servletHolder = new ServletHolder(servlet);

        context.addServlet(servletHolder, "/*");

        server.setHandler(context);

        server.start();

    }

    public static LambdaHTTPServer bind(final int port) throws Exception {
        final ServletRunner servletRunner = new ServletRunner(port);
        final ServletServer servletServer = new ServletServer();
        servletRunner.start(servletServer);

        servletServer.getEngine().registerStopCallback(() -> {
            servletRunner.stop();
            return "stopped jetty server";
        });
        return servletServer.getLambdaHTTPServer();
    }

    public void stop() {
        try {
            server.stop();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
