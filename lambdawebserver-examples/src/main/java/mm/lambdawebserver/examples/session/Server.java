package mm.lambdawebserver.examples.session;

import com.google.common.base.Strings;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.RequestHandler;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.extensions.sessions.Session;
import mm.lambdawebserver.impl.UndertowHttpServer;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public class Server {

    final static Function<Function<String, RequestHandler>, RequestHandler> AUTH =
            f -> r -> Session.from(r).get("user")
                    .map(user -> f.apply(user).applySafe(r))
                    .orElse(Session.put("msg", "please login first").addTo(Response.seeOther("/")));

    public static void main(final String[] args) throws IOException {
        final LambdaHTTPServer server = UndertowHttpServer.bind(80);

        Session.secret = "123456";

        server.get("/", r -> {
            final Optional<String> user = Session.from(r).get("user");
            final Optional<Response> alreadyLoggedIn = user.map(u -> Response.seeOther("/userpage"));
            final Response login = Pages.login(Session.from(r).getAndRemove("msg"));
            return alreadyLoggedIn.orElse(login);
        });

        server.post("/", r -> {

            final Map<String, String> body = Session.parseUrlEncodedKeyValuePairs(r.bodyAsString());

            final String username = body.get("username");
            final String password = body.get("password");

            if (!Strings.isNullOrEmpty(username) && username.equals(password)) {
                return Session.put("user", username).addTo(Response.seeOther("/userpage"));
            } else {
                return Session.from(r).with("msg", "login failed").addTo(Response.seeOther("/"));
            }
        });

        server.post("/logout", r -> Session.empty().addTo(Response.seeOther("/")));

        server.get("/userpage", AUTH.apply(user -> r -> Pages.userPage(user)));

        System.in.read();
    }

}
