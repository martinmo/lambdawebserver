package mm.lambdawebserver.examples.session;

import com.googlecode.jatl.Html;
import mm.lambdawebserver.api.Response;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Optional;

public class Pages {

    public static Response login(Optional<String> message) {
        return new LoginPage(message).toResponse();
    }

    public static Response userPage(String username) {
        return new PersonalPage(username).toResponse();
    }

    static abstract class AbstractPage {

        abstract String pageTitle();

        abstract void body(Html html);

        public byte[] generate() {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            final PrintWriter printer = new PrintWriter(outputStream);

            Html html = new Html(printer);

            html.html();

            html.head();
            html.title().text(pageTitle()).end();
            html.end();

            html.body();
            body(html);
            html.endAll();
            html.done();

            printer.flush();
            printer.close();
            return outputStream.toByteArray();
        }

        protected void logout(Html html) {
            html.form().action("/logout").method("POST");
            html.button().action("submit").text("Logout").end();
            html.end();
        }

        public Response toResponse() {
            return Response.bytes(generate()).asHTML();
        }
    }

    public static class PersonalPage extends AbstractPage {

        private final String username;

        public PersonalPage(String username) {
            this.username = username;
        }

        @Override
        String pageTitle() {
            return "Users page";
        }

        @Override
        void body(Html html) {
            html.div().classAttr("menu");
            logout(html);
            html.end();

            html.h1().text("Hi " + username + "!").end();
        }
    }

    public static class LoginPage extends AbstractPage {

        final Optional<String> message;

        public LoginPage(Optional<String> message) {
            this.message = message;
        }

        @Override
        String pageTitle() {
            return "Login";
        }

        @Override
        void body(Html html) {
            message.map(m -> html.div().text(m).end());

            html.form().action("/").method("POST");
            html.label().forAttr("username").text("Username").end();
            html.input().name("username").id("username").end();
            html.label().forAttr("password").text("Password").end();
            html.input().type("password").name("password").id("password").end();
            html.button().action("submit").text("Login").end();
            html.end();
        }


    }
}
