package mm.lambdawebserver.examples.resource;

import mm.lambdawebserver.api.Extractor;
import mm.lambdawebserver.api.Method;
import mm.lambdawebserver.api.RequestHandler;
import mm.lambdawebserver.api.Response;

public class ResourceHandler {

    // Resource -> RequestHandler
    static <ID, E> RequestHandler handle(Extractor<ID> extractor, Resource<ID, E> resource) {
        return r -> {
            if (r.method.equals(Method.GET) && r.fragment.isPresent()) {
                return GET(resource, extractor).apply(r);
            }
            if (r.method.equals(Method.GET) && !r.fragment.isPresent()) {
                return LIST(resource).apply(r);
            }
            if (r.method.equals(Method.POST)) {
                return CREATE(resource).apply(r);
            }
            if (r.method.equals(Method.PUT)) {
                return UPDATE(resource, extractor).apply(r);
            }
            if (r.method.equals(Method.DELETE)) {
                return DELETE(resource, extractor).apply(r);
            }


            return Response.notFound();
        };
    }

    /** helpers */

    static <ID, E> RequestHandler GET(Resource<ID, E> resource, Extractor<ID> extractor) {
        return Extractor.extract(extractor, (ID id) -> r -> Response.mayBeOk(resource.get(id)));
    }

    static <ID, E> RequestHandler LIST(Resource<ID, E> resource) {
        return r -> Response.ok(resource.list());
    }

    static <ID, E> RequestHandler CREATE(Resource<ID, E> resource) {
        return Extractor.extract(Extractor.JSON(resource.getResourceClass()),
                (E o) -> r -> resource.create(o).map(id -> Response.movedPermanently("" + id)).orElse(Response.error("error"))
        );
    }

    static <ID, E> RequestHandler UPDATE(Resource<ID, E> resource, Extractor<ID> extractor) {
        return Extractor.extract2(extractor, Extractor.JSON(resource.getResourceClass()),
                (ID id) -> (E o) -> r -> resource.update(id, o).map(u -> Response.ok(u)).orElse(Response.error("error"))
        );
    }

    static <ID, E> RequestHandler DELETE(Resource<ID, E> resource, Extractor<ID> extractor) {
        return Extractor.extract(extractor,
                (ID id) -> r -> Response.ok(resource.delete(id))
        );
    }
}