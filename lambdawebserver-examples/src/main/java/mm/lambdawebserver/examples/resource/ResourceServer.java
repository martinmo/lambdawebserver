package mm.lambdawebserver.examples.resource;

import mm.lambdawebserver.api.Extractor;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Method;
import mm.lambdawebserver.impl.UndertowHttpServer;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ResourceServer {
    static class User {
        public String name;

        public User(final String name) {
            this.name = name;
        }

        public User() {
        }

        @Override
        public String toString() {
            return "User{" + "name='" + name + '\'' + '}';
        }
    }

    protected static class UserResource implements Resource<String, User> {

        private Map<String, User> users = new ConcurrentHashMap<>();

        public UserResource() {
        }

        @Override
        public Class<User> getResourceClass() {
            return User.class;
        }

        @Override
        public Optional<String> create(User newObject) {
            final String id = new Date().getTime() + "";
            users.put(id, newObject);
            return Optional.of(id);
        }

        @Override
        public Optional<User> update(String id, User toUpdate) {
            User u = users.merge(id, toUpdate, (u1, u2) -> u2);
            return Optional.ofNullable(u);
        }

        @Override
        public Optional<User> get(String id) {
            return Optional.ofNullable(users.get(id));
        }

        @Override
        public List<User> list() {
            return new ArrayList<>(users.values());
        }

        @Override
        public Optional<String> delete(String id) {
            final User removed = users.remove(id);
            if (removed != null) {
                return Optional.of(id);
            } else {
                return Optional.empty();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        LambdaHTTPServer server = UndertowHttpServer.bind(80);
        final UserResource resource = new UserResource();
        User u = new User("martin");
        resource.create(u);

        server.handle(Method.ANY, "/users/*", ResourceHandler.handle(Extractor.STRING, resource));
    }


}
