package mm.lambdawebserver.examples.resourcecontext;

import mm.lambdawebserver.api.Extractor;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.RequestHandler;
import mm.lambdawebserver.api.Response;

public class ResourceHandler<ID, E> {

    public ResourceHandler(LambdaHTTPServer server, Extractor<ID> extractor, Resource<ID, E> resource) {

        server.get("/*", GET(resource, extractor));
        server.get("/", LIST(resource));
        server.put("/*", UPDATE(resource, extractor));
        server.post("/", CREATE(resource));
        server.delete("/*", DELETE(resource, extractor));
    }

    /**
     * helpers
     */

    static <ID, E> RequestHandler GET(Resource<ID, E> resource, Extractor<ID> extractor) {
        return Extractor.extract(extractor, (ID id) -> r -> Response.mayBeOk(resource.get(id)));
    }

    static <ID, E> RequestHandler LIST(Resource<ID, E> resource) {
        return r -> Response.ok(resource.list());
    }

    static <ID, E> RequestHandler CREATE(Resource<ID, E> resource) {
        return Extractor.extract(Extractor.JSON(resource.getResourceClass()),
                (E o) -> r -> resource.create(o).map(id -> Response.movedPermanently("" + id)).orElse(Response.error("error"))
        );
    }

    static <ID, E> RequestHandler UPDATE(Resource<ID, E> resource, Extractor<ID> extractor) {
        return Extractor.extract2(extractor, Extractor.JSON(resource.getResourceClass()),
                (ID id) -> (E o) -> r -> resource.update(id, o).map(u -> Response.ok(u)).orElse(Response.error("error"))
        );
    }

    static <ID, E> RequestHandler DELETE(Resource<ID, E> resource, Extractor<ID> extractor) {
        return Extractor.extract(extractor,
                (ID id) -> r -> Response.ok(resource.delete(id))
        );
    }
}