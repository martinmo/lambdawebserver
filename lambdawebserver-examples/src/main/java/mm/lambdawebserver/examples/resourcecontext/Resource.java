package mm.lambdawebserver.examples.resourcecontext;

import java.util.List;
import java.util.Optional;

// I == Class of Id/Key
// R == Class of Enities
interface Resource<ID, E> {

    Class<E> getResourceClass();

    // returns id
    Optional<ID> create(E newObject);

    Optional<E> update(ID id, E toUpdate);

    Optional<E> get(ID id);

    List<E> list();

    // id if removed
    Optional<ID> delete(ID id);
}
