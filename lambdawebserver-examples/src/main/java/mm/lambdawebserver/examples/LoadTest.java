package mm.lambdawebserver.examples;

import mm.lambdawebserver.api.Wrapper;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.impl.UndertowHttpServer;

import java.io.IOException;
import java.util.Random;

public class LoadTest {

    public static void main(String[] args) throws IOException {

        LambdaHTTPServer server = UndertowHttpServer.bind(80);

        server.around("/*", Wrapper.TIMING);
        server.get("/", r -> {
            try {
                Thread.sleep(new Random().nextInt(1001));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return Response.ok("ok");
        });

        server.get("/file", r -> Response.file("data.json").as("application/json"));


        System.in.read();

        server.stop();

    }

}
