package mm.lambdawebserver.examples.restlike.bookstore;

import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.api.Wrapper;
import mm.lambdawebserver.impl.UndertowHttpServer;

import java.io.IOException;

import static mm.lambdawebserver.api.Extractor.*;

public class BookServer {

    // threadsafe singleton service
    private final BookService bookService = new BookService();
    private final LambdaHTTPServer s;

    public BookServer(final int port) throws IOException {
        bookService.insert(BookService.Book.of("The Lord of the Rings", "John Ronald Reuel Tolkien",
                "When Mr. Bilbo Baggins of Bag End announced that he would shortly be celebrating " +
                "his eleventy-first birthday with a party of special magnificence, " +
                "there was much talk and excitement in Hobbiton. ..."
        ));

        s = UndertowHttpServer.bind(port);

        s.around("/*", Wrapper.TIMING);

        s.get("/books/", r -> Response.ok(bookService.all()));

        s.get("/books/*", extract(STRING, id -> r ->
                        bookService.find(id).map(Response::ok).orElse(Response.notFound())
        ));

        s.delete("/books/*", extract(STRING, id -> r -> {
            final boolean deleted = bookService.delete(id);
            if (deleted) {
                return Response.ok("deleted");
            } else {
                return Response.notFound("book with id " + id);
            }
        }));

        s.put("/books/*", extract2(STRING, JSON(BookService.Book.class),
                id -> book -> r -> {
                    bookService.update(id, book);
                    return Response.ok("updated");
                }
        ));

        s.post("/books/", extract(JSON(BookService.Book.class), book -> r -> {
            final BookService.Book newBook = bookService.insert(book);
            return Response.movedPermanently("/books/" + newBook.id);
        }));

        // this serves an AngularJS app
        s.get("/", r -> Response.file("mm/lambdawebserver/examples/restlike/bookstore/index.html"));
    }

    public void stop() {
        s.stop();
    }

    public static void main(final String[] args) throws IOException {
        final BookServer bookServer = new BookServer(80);
        System.in.read();
        bookServer.stop();
    }
}
