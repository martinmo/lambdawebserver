package mm.lambdawebserver.examples.restlike.bookstore;

import java.util.*;

public class BookService {

    public static class Book {
        public String id;
        public String title;
        public String author;
        public String content;

        public static Book of(String title, String author, String content) {
            Book b = new Book();
            b.title = title;
            b.author = author;
            b.content = content;
            return b;
        }
    }

    private Map<String, Book> bookStore = Collections.synchronizedMap(new HashMap<>());

    public Optional<Book> find(final String id) {
        return Optional.ofNullable(bookStore.get(id));
    }

    public void update(final String id, final Book book){
        bookStore.put(id, book);
    }

    public Book insert(final Book book){
        book.id = UUID.randomUUID().toString();
        bookStore.put(book.id, book);
        return book;
    }

    public List<Book> all(){
        final ArrayList<Book> books = new ArrayList<>();
        books.addAll(bookStore.values());
        return Collections.unmodifiableList(books);
    }

    public boolean delete(final String id){
        final Book removed = bookStore.remove(id);
        return removed != null;
    }

}
