package mm.lambdawebserver.examples.reflectioninjection;

import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.RequestHandler;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.impl.UndertowHttpServer;

import java.io.IOException;
import java.util.function.Supplier;

public class Server {

    /**
     * see InjectionTest.java
     */
    public static void main(String[] args) throws IOException {

        LambdaHTTPServer server = UndertowHttpServer.bind(80);

        Supplier<RequestHandler> instanceSupplier = () -> {
            // somehow prepare an instance of the request handler
            System.out.println("preparing instance");
            return r -> Response.ok("ok");
        };

        server.get("/", r -> instanceSupplier.get().apply(r));

    }
}
