package mm.lambdawebserver.examples.db;

import mm.lambdawebserver.extensions.db.DB;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public class LinkDao {

    public static void initLinks(Connection connection) {
        DB.update(connection, "drop table IF EXISTS links");
        DB.update(connection, "create table links ( url VARCHAR(128), name VARCHAR(128));");

        insert(connection, new Link("http://google.de","google"));
        insert(connection, new Link("http://google.com","google us"));
        insert(connection, new Link("url","name"));

        list(connection).forEach(l -> System.out.println(l.getName() + " " + l.getUrl()));
    }

    public static List<Link> list(Connection connection) {
        return DB.queryMultiple(connection, Link.class, "select * from links");
    }

    public static Optional<Link> find(final Connection connection, final String name) {
        Link link = DB.querySingle(connection, Link.class, "select * from links where name = ?", name);
        return Optional.ofNullable(link);
    }

    public static void insert(Connection con, Link link){
        DB.update(con, "insert into links values (?, ?)", link.url, link.name);
    }
}
