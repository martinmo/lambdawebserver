package mm.lambdawebserver.examples.db;

public class Link {
    public String url;
    public String name;

    public Link(){}

    public Link(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
