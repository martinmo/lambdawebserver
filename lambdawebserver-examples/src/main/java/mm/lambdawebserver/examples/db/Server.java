package mm.lambdawebserver.examples.db;

import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.api.Wrapper;
import mm.lambdawebserver.extensions.db.DB;
import mm.lambdawebserver.extensions.db.DbWrapper;
import mm.lambdawebserver.impl.UndertowHttpServer;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

import static mm.lambdawebserver.api.Extractor.STRING;
import static mm.lambdawebserver.api.Extractor.extract;

public class Server {

    LambdaHTTPServer server;

    public Server() throws IOException, SQLException {
        server = UndertowHttpServer.bind(80);
        final DataSource dataSource = DB.init();

        DB.con(dataSource, c -> {
            LinkDao.initLinks(c);
            return Void.TYPE;
        });

        final DbWrapper withDb = DbWrapper.db(dataSource);

        server.around("/*", Wrapper.TIMING);
        server.get("/links", withDb.apply(
                connection -> r -> Response.ok(LinkDao.list(connection))
        ));
        server.get("/links/*", extract(STRING,
                i -> withDb.apply(con -> r -> LinkDao.find(con, i).map(link -> Response.ok(link)).orElse(Response.notFound()))
        ));

    }

    public static void main(final String[] args) throws IOException, SQLException {
        final Server server1 = new Server();
        System.in.read();
        server1.server.stop();
    }

}
