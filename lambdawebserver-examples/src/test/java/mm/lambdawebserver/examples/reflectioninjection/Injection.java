package mm.lambdawebserver.examples.reflectioninjection;

import javax.inject.Inject;
import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedAction;

// based on https://github.com/AdamBien/afterburner.fx/blob/2ecd4e33e6b4a7af666604944c3de6fc3047142c/src/main/java/com/airhacks/afterburner/injection/Injector.java
public class Injection {

    public static <C> C inject(Class<C> clazz){
        final C target = newInstance(clazz);

        Field[] fields = clazz.getDeclaredFields();
        for (final Field field : fields) {
            if (field.isAnnotationPresent(Inject.class)) {
                final Object value = newInstance(field.getType());
                injectIntoField(field,target, value );
            }
        }
        return target;
    }

    static <T> T newInstance(Class<T> clazz){
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    static void injectIntoField(final Field field, final Object instance, final Object target) {
        AccessController.doPrivileged((PrivilegedAction) () -> {
            boolean wasAccessible = field.isAccessible();
            try {
                field.setAccessible(true);
                field.set(instance, target);
                return null; // return nothing...
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                throw new IllegalStateException("Cannot set field: " + field, ex);
            } finally {
                field.setAccessible(wasAccessible);
            }
        });
    }

}
