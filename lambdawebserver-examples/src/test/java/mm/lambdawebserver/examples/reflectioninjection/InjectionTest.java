package mm.lambdawebserver.examples.reflectioninjection;

import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.RequestHandler;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.impl.UndertowHttpServer;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;

import static com.jayway.restassured.RestAssured.get;
import static mm.lambdawebserver.examples.reflectioninjection.Injection.inject;
import static org.hamcrest.Matchers.equalTo;

public class InjectionTest {

    @Test
    public void canInject() throws IOException {
        LambdaHTTPServer server = UndertowHttpServer.bind(8123);

        server.get("/", inject(InjectedHandler.class).get());

        get("http://localhost:8123/").then().assertThat().body(equalTo("serving"));

        server.stop();
    }

    public static class InjectedHandler {

        @Inject
        private Service service;

        public RequestHandler get() {
            return r -> Response.ok(service.serve());
        }
    }


    public static class Service {

        public String serve() {
            return "serving";
        }
    }
}