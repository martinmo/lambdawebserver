package mm.lambdawebserver.examples.assertonrequest;

import mm.lambdawebserver.api.Request;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class UriChecker extends TypeSafeMatcher<Request> {

    private final String uri;

    public UriChecker(String uri) {
        this.uri = uri;
    }

    @Override
    protected boolean matchesSafely(Request request) {
        return request.uri.toString().equals(uri);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("uri is " + uri);
    }

    public static UriChecker uri(String uri) {
        return new UriChecker(uri);
    }
}
