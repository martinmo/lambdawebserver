package mm.lambdawebserver.examples.assertonrequest;

import mm.lambdawebserver.api.Method;
import mm.lambdawebserver.api.Request;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class MethodChecker extends TypeSafeMatcher<Request> {

    private final Method method;

    public MethodChecker(Method method) {
        this.method = method;
    }

    @Override
    protected boolean matchesSafely(Request request) {
        return request.method.matches(method);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("method is " + method.name());
    }

    public static MethodChecker method(Method method) {
        return new MethodChecker(method);
    }
}
