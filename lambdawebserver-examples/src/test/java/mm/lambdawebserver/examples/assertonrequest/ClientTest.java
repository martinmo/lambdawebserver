package mm.lambdawebserver.examples.assertonrequest;

import com.jayway.restassured.RestAssured;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Method;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.impl.UndertowHttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.jayway.restassured.RestAssured.get;
import static mm.lambdawebserver.examples.assertonrequest.MethodChecker.method;
import static mm.lambdawebserver.examples.assertonrequest.UriChecker.uri;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class ClientTest {

    LambdaHTTPServer server;
    RequestRecorder recorder;

    @Before
    public void before() throws IOException {
        server = UndertowHttpServer.bind(18077);
        RestAssured.port = 18077;
        recorder = new RequestRecorder();
        server.around("/*", recorder);
    }

    @After
    public void stop() {
        server.stop();
    }

    @Test
    public void clientSendsGetRequestWithUri() {
        server.get("/*", r -> Response.ok("ok"));

        get("/some").then().assertThat().body(equalTo("ok"));

        assertThat(recorder.requests, hasItem(both(method(Method.GET)).and(uri("/some"))));

        assertThat(recorder.first(), not(method(Method.POST)));
    }
}
