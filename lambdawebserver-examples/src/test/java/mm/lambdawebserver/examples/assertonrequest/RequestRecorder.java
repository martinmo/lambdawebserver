package mm.lambdawebserver.examples.assertonrequest;

import mm.lambdawebserver.api.Request;
import mm.lambdawebserver.api.RequestHandler;
import mm.lambdawebserver.api.Wrapper;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class RequestRecorder implements Wrapper {

    public List<Request> requests = Collections.synchronizedList(new LinkedList<>());

    public Request first(){
        if(requests.size() > 1){
            throw new IllegalStateException("no request was recorded");
        }
        return requests.get(0);
    }

    @Override
    public RequestHandler around(RequestHandler handler) {
        return r -> {
            requests.add(r);
            return handler.apply(r);
        };
    }

}
