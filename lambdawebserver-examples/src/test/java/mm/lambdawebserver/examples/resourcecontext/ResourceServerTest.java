package mm.lambdawebserver.examples.resourcecontext;

import com.jayway.restassured.RestAssured;
import mm.lambdawebserver.api.Extractor;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Wrapper;
import mm.lambdawebserver.impl.UndertowHttpServer;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.arrayContainingInAnyOrder;
import static org.junit.Assert.assertThat;

public class ResourceServerTest {

    private LambdaHTTPServer server;
    private static final AtomicInteger port = new AtomicInteger(9010);
    private static final String path = "/users/";


    @Before
    public void before() throws IOException {
        int currentPort = port.getAndIncrement();
        RestAssured.port = currentPort;
        server = UndertowHttpServer.bind(currentPort);

        final ResourceServer.UserResource userResource = new ResourceServer.UserResource();

        new ResourceHandler(server.context("/users"), Extractor.STRING, userResource);

        server.around("/*", Wrapper.TIMING);
    }

    ResourceServer.User user1 = new ResourceServer.User("user1");
    ResourceServer.User user2 = new ResourceServer.User("user2");

    @Test
    public void canInsert() {
        final String id1 = given().body(user1).when().post(path).header("Location");
        final String id2 = given().body(user2).when().post(path).header("Location");

        final ResourceServer.User[] users = get(path).as(ResourceServer.User[].class);
        LoggerFactory.getLogger(this.getClass()).info("users {}", users);
        assertThat(users, arrayContainingInAnyOrder(UserM.name("user1"), UserM.name("user2")));
    }

    @Test
    public void canDelete() {
        final String id1 = given().body(user1).when().post(path).header("Location");
        final String id2 = given().body(user2).when().post(path).header("Location");

        System.out.println(id1);

        delete(path + id1).then().assertThat().statusCode(200);

        final ResourceServer.User[] users = get(path).as(ResourceServer.User[].class);
        LoggerFactory.getLogger(this.getClass()).info("users {}", users);
        assertThat(users, arrayContainingInAnyOrder(UserM.name("user2")));
    }

    @Test
    public void canUpdate() {
        final String id1 = given().body(user1).when().post(path).header("Location");
        final String id2 = given().body(user2).when().post(path).header("Location");

        ResourceServer.User other = new ResourceServer.User("otherName");
        given().body(other).when().put(path + id1).then().assertThat().statusCode(200);

        final ResourceServer.User[] users = get(path).as(ResourceServer.User[].class);
        LoggerFactory.getLogger(this.getClass()).info("users {}", users);
        assertThat(users, arrayContainingInAnyOrder(UserM.name("user2"), UserM.name("otherName")));
    }


    @After
    public void stop() {
        server.stop();
    }

    private static final class UserM extends BaseMatcher<ResourceServer.User> {
        private final String name;

        private UserM(String name) {
            this.name = name;
        }

        public static UserM name(final String name) {
            return new UserM(name);
        }

        @Override
        public boolean matches(Object o) {
            final ResourceServer.User user = (ResourceServer.User) o;
            return name.equals(user.name);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("name to match is " + name);
        }
    }
}