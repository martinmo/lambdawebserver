package mm.lambdawebserver.extensions.db;

import mm.lambdawebserver.api.RequestHandler;
import mm.lambdawebserver.api.Response;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.Function;

public interface DbWrapper {
    RequestHandler apply(Function<Connection, RequestHandler> dbHandler);

    /**
     * DbWrapper
     * ^=
     * Function<Function<Connection, RequestHandler>, RequestHandler>
     */
    public static DbWrapper db(DataSource ds) {
        return dbHandler -> (request) -> {
            try {
                try (Connection connection = ds.getConnection()) {
                    try {
                        connection.setAutoCommit(false);
                    } catch (SQLException e) {
                        return Response.error(e.getMessage());
                    }
                    final Response response = dbHandler.apply(connection).apply(request);
                    try {
                        if (response.code >= 500 && response.code < 600) {
                            connection.rollback();
                        } else {
                            connection.commit();
                        }
                    } catch (SQLException e) {
                        Response.error("failed to commit/rollback");
                    }

                    return response;
                }
            } catch (SQLException e) {
                return Response.error("failed to get connection");
            }
        };
    }
}
