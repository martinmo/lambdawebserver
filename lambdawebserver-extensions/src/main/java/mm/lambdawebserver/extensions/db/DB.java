package mm.lambdawebserver.extensions.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.AbstractListHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.function.Function;


public class DB {

    public static DataSource init() {
        try {
            try (InputStream resourceAsStream = DB.class.getResourceAsStream("/db.properties")) {
                Properties props = new Properties();
                props.load(resourceAsStream);
                HikariConfig config = new HikariConfig(props);
                return new HikariDataSource(config);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("failed to setup db", e);
        }
    }

    public static DataSource init(final String driverName, final String jdbcUrl) {
        Properties props = new Properties();
        props.setProperty("dataSourceClassName", driverName);
        props.setProperty("dataSource.url", jdbcUrl);
        HikariConfig config = new HikariConfig(props);
        return new HikariDataSource(config);
    }

    public static DataSource initHSQLMemDB() {
        return init("org.hsqldb.jdbc.JDBCDataSource", "jdbc:hsqldb:mem:inMemDB");
    }

    public static DataSource initHSQLFileDB(final String dir) {
        return init("org.hsqldb.jdbc.JDBCDataSource", "jdbc:hsqldb:file:" + dir);
    }

    public static void close(DataSource dataSource) {
        if (dataSource instanceof HikariDataSource) {
            ((HikariDataSource) dataSource).close();
        } else {
            throw new RuntimeException("cannot close none HikariDataSource");
        }
    }

    public static <T> T query(final Connection con, final SafeFunction<ResultSet, T> handler, final String query, Object... args) {
        final QueryRunner queryRunner = new QueryRunner();
        try {
            final ResultSetHandler<T> resultSetHandler = rs -> wrapSafeFunction(handler).apply(rs);
            return queryRunner.query(con, query, resultSetHandler, args);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static <T> List<T> queryMultiplePlain(final Connection con, final SafeFunction<ResultSet, T> handler, final String query, Object... args) {
        final QueryRunner queryRunner = new QueryRunner();
        try {

            final AbstractListHandler<T> listHandler = new AbstractListHandler<T>() {
                @Override
                protected T handleRow(ResultSet rs) throws SQLException {
                    return wrapSafeFunction(handler).apply(rs);
                }
            };

            return queryRunner.query(con, query, listHandler, args);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static int update(final Connection con, final String query, Object... args) {
        final QueryRunner queryRunner = new QueryRunner();
        try {
            return queryRunner.update(con, query, args);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static <T> T querySingle(final Connection con, final Class<T> clazz, final String query, Object... args) {
        final QueryRunner queryRunner = new QueryRunner();
        try {
            return queryRunner.query(con, query, new BeanHandler<>(clazz), args);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static <T> List<T> queryMultiple(final Connection con, final Class<T> clazz, final String query, Object... args) {
        final QueryRunner queryRunner = new QueryRunner();
        try {
            return queryRunner.query(con, query, new BeanListHandler<>(clazz), args);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static <T> T con(DataSource dataSource, SafeFunction<Connection, T> connectionConsumer) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            final T result = connectionConsumer.apply(connection);
            connection.commit();
            return result;
        } catch (Throwable e) {
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
    }

    public static DbInstance prepare(DataSource dataSource) {
        return new DbInstance(dataSource);
    }

    public static class DbInstance {
        final DataSource dataSource;

        private DbInstance(DataSource dataSource) {
            this.dataSource = dataSource;
        }

        public <T> T withConnection(SafeFunction<Connection, T> f) {
            return con(dataSource, f);
        }

        public <T> T tx(SafeFunction<ConnectionWrapper, T> f) {
            return con(dataSource, c -> f.apply(new ConnectionWrapper(c)));
        }
    }

    public static class ConnectionWrapper {
        final public Connection connection;

        public ConnectionWrapper(Connection connection) {
            this.connection = connection;
        }

        public <T> T query(final SafeFunction<ResultSet, T> handler, final String query, Object... args) {
            return DB.query(connection, handler, query, args);
        }

        public <T> List<T> queryMultiplePlain(final SafeFunction<ResultSet, T> handler, final String query, Object... args) {
            return DB.queryMultiplePlain(connection, handler, query, args);
        }

        public int update(final String query, Object... args) {
            return DB.update(connection, query, args);
        }

        public <T> T querySingle(final Class<T> clazz, final String query, Object... args) {
            return DB.querySingle(connection, clazz, query, args);
        }

        public <T> List<T> queryMultiple(final Class<T> clazz, final String query, Object... args) {
            return DB.queryMultiple(connection, clazz, query, args);
        }
    }

    public static interface SafeFunction<A, B> {
        B apply(A a) throws Throwable;
    }

    public static <A, B> Function<A, B> wrapSafeFunction(SafeFunction<A, B> safeFunction) {
        return (t) -> {
            try {
                return safeFunction.apply(t);
            } catch (Throwable throwable) {
                throw new RuntimeException(throwable);
            }
        };
    }
}