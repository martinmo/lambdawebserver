package mm.lambdawebserver.extensions.sessions;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import mm.lambdawebserver.api.Request;
import mm.lambdawebserver.api.Response;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import java.io.UnsupportedEncodingException;
import java.net.HttpCookie;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

//@Deprecated // this is probably not secure
// better have a look at
// play1 https://github.com/playframework/play1/blob/master/framework/src/play/mvc/Scope.java
// or https://github.com/lawrence0819/java-stateless-http-session
public class Session {

    private final Map<String, String> values = new HashMap<>();

    public final static String SESSION = "_ls";
    public static String secret = UUID.randomUUID().toString();

    public Session(final Map<String, String> values) {
        this.values.putAll(values);
    }

    public Session() {}

    public static Session put(String key, String value){
        return new Session().with(key, value);
    }

    public static Session empty(){
        return new Session();
    }

    public static Session from(Request r) {
        return r.cookie(SESSION).flatMap(c -> fromCookie(c)).orElse(new Session());
    }

    public Response addTo(Response response){
        return response.withCookie(toCookie());
    }

    public Optional<String> getAndRemove(String key){
        final Optional<String> value = Optional.ofNullable(values.get(key));
        values.remove(key);
        return value;
    }

    public Optional<String> get(final String key){
        return Optional.ofNullable(values.get(key));
    }

    public Session with(final String key, final String value){
        values.put(key, value);
        return this;
    }

    public static Map<String, String> parseUrlEncodedKeyValuePairs(String string){

        if(Strings.isNullOrEmpty(string)){
            return new HashMap<>();
        }

        final Map<String, String> values = Splitter.on('&').trimResults().withKeyValueSeparator('=').split(string);

        final Map<String, String> unescaped = new HashMap<>();
        values.forEach((k,v) ->{
            try {
                final String key = URLDecoder.decode(k, "UTF-8");
                final String value = URLDecoder.decode(v, "UTF-8");
                unescaped.put(key, value);
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        });

        return unescaped;
    }

    public static Optional<Session> fromCookie(final HttpCookie cookie) {
        try {
            final String encrypted = cookie.getValue();
            StandardPBEStringEncryptor textEncryptor = new StandardPBEStringEncryptor();
            textEncryptor.setPassword(secret);

            final String decryptedLine = textEncryptor.decrypt(encrypted);

            final Map<String, String> map = parseUrlEncodedKeyValuePairs(decryptedLine);

            return Optional.of(new Session(map));
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public HttpCookie toCookie() {

        Map<String, String> escaped = new HashMap<>();
        values.forEach((k,v) ->{
            try {
                final String key = URLEncoder.encode(k, "UTF-8");
                final String value = URLEncoder.encode(v, "UTF-8");
                escaped.put(key,value);
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        });

        final String joined = Joiner.on('&').withKeyValueSeparator("=").join(escaped);
        StandardPBEStringEncryptor textEncryptor = new StandardPBEStringEncryptor();
        textEncryptor.setPassword(secret);

        final String encrypted = textEncryptor.encrypt(joined);

        return new HttpCookie(SESSION, encrypted);
    }

    protected Map<String, String> getMap(){
        return values;
    }

}
