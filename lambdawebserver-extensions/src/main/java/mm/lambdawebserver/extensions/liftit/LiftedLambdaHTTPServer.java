package mm.lambdawebserver.extensions.liftit;

import mm.lambdawebserver.api.Method;
import mm.lambdawebserver.api.Response;

import java.util.function.Function;

public interface LiftedLambdaHTTPServer<REQ, RESP> {
    void get(String path, Function<REQ, RESP> handler);

    void post(String path, Function<REQ, RESP> handler);

    void delete(String path, Function<REQ, RESP> handler);

    void put(String path, Function<REQ, RESP> handler);

    void handle(Method method, String path, Function<REQ, RESP> handler);

    void onError(Function<Throwable, Response> catcher);

    void stop();
}
