package mm.lambdawebserver.extensions.liftit;

import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Method;
import mm.lambdawebserver.api.Request;
import mm.lambdawebserver.api.RequestHandler;
import mm.lambdawebserver.api.Response;

import java.util.function.Function;

public final class Lift {

    public static <REQ> LiftedLambdaHTTPServer<REQ, Response> liftBefore(LambdaHTTPServer server, Function<Request, PrepareResult<REQ>> prepare) {
        return lift(server, prepare, Function.<Response>identity());
    }
    public static <RESP> LiftedLambdaHTTPServer<Response, RESP> liftAfter(LambdaHTTPServer server, Function<RESP, Response> postProcess) {
        return lift(server, r -> PrepareResult.success(r), postProcess);
    }

    public static <REQ, RESP> LiftedLambdaHTTPServer<REQ, RESP> lift(LambdaHTTPServer server, Function<Request, PrepareResult<REQ>> prepare, Function<RESP, Response> postProcess) {

        Function<Function<REQ, RESP>, RequestHandler> transform = handler -> r -> {
            final PrepareResult<REQ> result = prepare.apply(r);
            if (result.isSuccess()) {
                return postProcess.apply(handler.apply(result.success));
            } else {
                return result.failure;
            }
        };

        final LiftedLambdaHTTPServer lifted = new LiftedLambdaHTTPServer<REQ, RESP>() {

            @Override
            public void get(String path, Function<REQ, RESP> handler) {
                server.get(path,transform.apply(handler));
            }

            @Override
            public void post(String path, Function<REQ, RESP> handler) {
                server.post(path, transform.apply(handler));
            }

            @Override
            public void delete(String path, Function<REQ, RESP> handler) {
                server.delete(path, transform.apply(handler));
            }

            @Override
            public void put(String path, Function<REQ, RESP> handler) {
                server.put(path, transform.apply(handler));
            }

            @Override
            public void handle(Method method, String path, Function<REQ, RESP> handler) {
                server.handle(method, path, transform.apply(handler));
            }

            @Override
            public void onError(Function<Throwable, Response> catcher) {
                server.onError(e -> r -> catcher.apply(e));
            }

            @Override
            public void stop() {
                server.stop();
            }
        };
        return lifted;
    }

    public static class PrepareResult<REQ>{
        private final REQ success;
        private final Response failure;
        private PrepareResult(REQ success, Response failure) {
            this.success = success;
            this.failure = failure;
        }
        public boolean isSuccess(){return success != null;}
        public static <REQ> PrepareResult success(REQ req){
            return new PrepareResult<>(req, null);
        }
        public static <REQ> PrepareResult failure(Response response){
            return new PrepareResult<>(null, response);
        }
    }
}
