package mm.lambdawebserver.extensions.liftit;

import com.jayway.restassured.RestAssured;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Method;
import mm.lambdawebserver.api.Request;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.impl.UndertowHttpServer;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

public class LiftTest {

    @Test
    public void canLift() throws IOException {
        final LambdaHTTPServer server = UndertowHttpServer.bind(8765);

        final Function<Request, Lift.PrepareResult<Method>> prepare = r -> Lift.PrepareResult.success(r.method);
        final LiftedLambdaHTTPServer<Method, Response> lifted = Lift.liftBefore(server.context("/test"), prepare);

        lifted.get("/*", m -> Response.ok(m.name()));

        RestAssured.get("http://localhost:8765/test/some").then().assertThat().body(Matchers.equalTo("GET"));

        server.stop();
    }

    public static class AuthRequest extends Request {
        public AuthRequest(Request r, String username) {
            super(r.method, r.uri, r.body, r.headers, r.fragment, r.params);
            this.username = username;
        }

        private final String username;

        public String getUsername() {
            return username;
        }
    }

    @Test
    public void canAuth() throws IOException {
        final LambdaHTTPServer server = UndertowHttpServer.bind(8766);

        Function<Request, Lift.PrepareResult<AuthRequest>> prepare = r -> {
            final Optional<String> username = r.headers.getFirst("Auth");
            return username.map(u -> Lift.PrepareResult.success(new AuthRequest(r, u))).orElse(Lift.PrepareResult.failure(Response.code(401)));
        };

        final LiftedLambdaHTTPServer<AuthRequest, Response> lifted = Lift.liftBefore(server, prepare);

        lifted.get("/*", m -> Response.ok(m.getUsername()));

        RestAssured.get("http://localhost:8766/test").then().assertThat().statusCode(401);

        RestAssured.given().headers("Auth", "admin").get("http://localhost:8766/test").then().assertThat().statusCode(200).and().body(Matchers.equalTo("admin"));

        server.stop();
    }

}
