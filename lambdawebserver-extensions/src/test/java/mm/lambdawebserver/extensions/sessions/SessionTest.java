package mm.lambdawebserver.extensions.sessions;

import com.google.common.collect.ImmutableMap;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.net.HttpCookie;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class SessionTest {

    @Test
    public void canEncrypt(){
        final ImmutableMap<String, String> map = ImmutableMap.<String, String>builder().put("key", "value").build();

        final Session session = new Session(map);

        final HttpCookie cookie = session.toCookie();

        System.out.println(cookie.toString());

        final Optional<Session> session2 = Session.fromCookie(cookie);

        assertThat(session2.get().get("key").get(), equalTo("value"));
    }

    @Test
    public void checkValues(){
        final ImmutableMap<String, String> map = ImmutableMap.<String, String>builder()
                .put("key", "value")
                .put("1","=")
                .put("=","1")
                .build();
        canHandle(map);

        final ImmutableMap<String, String> map2 = ImmutableMap.<String, String>builder()
                .put("v1", "?")
                .put("v2", "..")
                .put("v3", "/")
                .build();
        canHandle(map2);
    }

    @Test
    public void checkLongText(){

        final StringBuilder stringBuilder = IntStream.of(2000).boxed()
                .reduce(new StringBuilder(),
                        (acc, i) -> acc.append("a"),
                        (s1, s2) -> s1.append(s2)
                );

        final ImmutableMap<String, String> map = ImmutableMap.<String, String>builder()
                .put("key", stringBuilder.toString())
                .build();
        canHandle(map);
    }

    private void canHandle(final Map<String,String> values){
        final ImmutableMap<String, String> map = ImmutableMap.<String, String>builder().putAll(values).build();

        final Session session = new Session(map);

        final HttpCookie cookie = session.toCookie();

        System.out.println(cookie.toString());

        final Optional<Session> session2 = Session.fromCookie(cookie);

        final Map<String, String> resultMap = session2.get().getMap();

        assertThat(resultMap, CoreMatchers.equalTo(map));
    }



}