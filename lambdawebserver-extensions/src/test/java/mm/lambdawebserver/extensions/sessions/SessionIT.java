package mm.lambdawebserver.extensions.sessions;

import com.jayway.restassured.response.Cookies;
import mm.lambdawebserver.api.LambdaHTTPServer;
import mm.lambdawebserver.api.Response;
import mm.lambdawebserver.it.AbstractServerTest;
import org.junit.Test;

import java.util.function.Function;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class SessionIT extends AbstractServerTest{

    public SessionIT(Function<Integer, LambdaHTTPServer> serverBuilder) {
        super(serverBuilder);
    }

    @Test
    public void canUseSession() {
        server.get("/", r -> Session.from(r).with("k", "v").addTo(Response.ok("ok")));
        server.get("/c", r ->
                Session.from(r).get("k").map(v -> Response.ok(v))
                        .orElse(Response.error("fail"))
        );

        final Cookies cookies = get("/").detailedCookies();

        given().cookies(cookies).get("/c").then().assertThat().body(equalTo("v"));
        given().get("/c").then().assertThat().body(equalTo("fail"));
    }

    @Test
    public void canCorruptSession() {
        server.get("/", r -> Session.from(r).with("k", "v").addTo(Response.ok("ok")));
        server.get("/c", r ->
                        Session.from(r).get("k").map(v -> Response.ok(v))
                                .orElse(Response.error("fail"))
        );

        given().cookie("_ls","hehe").get("/c").then().assertThat().body(equalTo("fail"));
        given().header("Cookie", "_ls=\"D9L3vDIShgAcUDsRTWz5wg==\"").get("/c").then().assertThat().body(equalTo("fail"));
    }




}