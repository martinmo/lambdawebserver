package mm.lambdawebserver.extensions.db;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

public class DbTest {

    DataSource dataSource;

    DB.DbInstance dbi;

    @Before
    public void before() {
        dataSource = DB.initHSQLMemDB();
        dbi = DB.prepare(dataSource);

        dbi.tx(db -> {
            db.update("CREATE TABLE users ( " +
                            "id VARCHAR(128) NOT NULL, " +
                            "username VARCHAR(128) NOT NULL, " +
                            "updated BIGINT NOT NULL, " +
                            "PRIMARY KEY (id)" +
                            ");"
            );

            db.update("CREATE TABLE notes ( " +
                            "id VARCHAR(128) NOT NULL, " +
                            "content VARCHAR(6000) NOT NULL, " +
                            "updated BIGINT NOT NULL, " +
                            "user_id VARCHAR(128), " +
                            "PRIMARY KEY (id), " +
                            "FOREIGN KEY (user_id) REFERENCES users(id));"
            );

            db.update("INSERT INTO users values (?,?,?)", "1", "admin", System.currentTimeMillis());
            db.update("INSERT INTO notes values (?,?,?,?)", "1", "content", System.currentTimeMillis(), "1");
            return Void.TYPE;
        });
    }

    @After
    public void after() {
        dbi.withConnection(c -> {
            DB.update(c, "DROP TABLE notes;");
            DB.update(c, "DROP TABLE users;");
            return Void.TYPE;
        });
    }

    @Test
    public void basicQuery() {
        dbi.withConnection(c -> {
            final String contents = DB.query(c, rs -> {
                rs.next();
                return rs.getString(2);
            }, "select * from notes");
            assertThat(contents, equalTo("content"));
            return Void.TYPE;
        });
    }

    @Test
    public void basicMultipleQuery() {
        final List<String> contents = dbi.withConnection(c -> DB.queryMultiplePlain(c, rs -> rs.getString(2), "select * from notes"));

        assertThat(contents, CoreMatchers.hasItems("content"));
    }

    public static final class Note {
        private String id;
        private String content;
        private long updated;
        private String userId;

        public Note() {
        }

        public Note(String id, String content, long updated, String userId) {
            this.id = id;
            this.content = content;
            this.updated = updated;
            this.userId = userId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public long getUpdated() {
            return updated;
        }

        public void setUpdated(long updated) {
            this.updated = updated;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }

    @Test
    public void basicQueryToUser() {
        final ArrayList<Note> notes = dbi.tx(db -> {

            db.update("INSERT INTO notes values (?,?,?,?)", "2", "content2", System.currentTimeMillis(), "1");

            return db.query(rs -> {
                final ArrayList<Note> ns = new ArrayList<>();
                while (rs.next()) {
                    final String id = rs.getString("id");
                    final String content = rs.getString("content");
                    final long updated = rs.getLong("updated");
                    final String user_id = rs.getString("user_id");
                    ns.add(new Note(id, content, updated, user_id));
                }
                return ns;
            }, "select * from notes");
        });

        assertThat(notes, hasSize(2));
    }

    @Test
    public void basicQueryMultiToUser() {
        final List<Note> notes = dbi.tx(db -> {

            db.update("INSERT INTO notes values (?,?,?,?)", "2", "content2", System.currentTimeMillis(), "1");

            return db.queryMultiplePlain(rs -> {
                final String id = rs.getString("id");
                final String content = rs.getString("content");
                final long updated = rs.getLong("updated");
                final String user_id = rs.getString("user_id");
                return new Note(id, content, updated, user_id);
            }, "select * from notes");
        });

        assertThat(notes, hasSize(2));
    }

    @Test
    public void basicMultipleBeansQuery() {
        final List<Note> notes = dbi.tx(c -> c.queryMultiple(Note.class, "select * from notes"));

        assertThat(notes, hasSize(1));
        assertThat(notes.get(0).content, equalTo("content"));
    }

    @Test
    public void join() {
        final List<Note> admins = dbi.tx(c -> c.queryMultiple(
                        Note.class,
                        "select n.* from notes n, users u where n.user_id = u.id and u.username = ?",
                        "admin")
        );
        assertThat(admins.get(0).content, equalTo("content"));
    }

    @Test
    public void tx() {
        try {
            dbi.tx(c -> {
                c.update("delete from notes");
                throw new RuntimeException("rollback");
            });
        } catch (RuntimeException e) {
        }

        final long count = dbi.tx(c -> c.query(rs -> {
            rs.next();
            return rs.getLong(1);
        }, "select count(*) from notes"));

        assertThat(count, equalTo(1L));
    }

//    @Test
//    public void array(){
//        dbi.withConnection(c -> {
//
//            final PreparedStatement statement = c.prepareStatement("select n.* from notes n where n.user_id in (?)");
//
//            final Array array = c.createArrayOf("VARCHAR", new String[]{"1"});
//
//            statement.setArray(1, array);
//
//            final ResultSet resultSet = statement.executeQuery();
//            resultSet.next();
//
//            assertThat(resultSet.getString("content"), equalTo("content"));
//            return Void.TYPE;
//        });
//    }

}
